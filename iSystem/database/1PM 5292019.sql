-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2019 at 06:59 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbinventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `bundle`
--

CREATE TABLE `bundle` (
  `id` int(11) NOT NULL,
  `bundle_code` varchar(512) NOT NULL,
  `bundle_description` varchar(512) NOT NULL,
  `system_unit` tinyint(4) NOT NULL DEFAULT '0',
  `allow_borrow` tinyint(4) NOT NULL DEFAULT '0',
  `location_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bundle`
--

INSERT INTO `bundle` (`id`, `bundle_code`, `bundle_description`, `system_unit`, `allow_borrow`, `location_id`, `active`) VALUES
(1, '123', 'Testing', 0, 0, 2, 0),
(2, '12312', 'tes', 0, 0, 2, 1),
(3, 'asd', 'asd', 0, 0, 2, 1),
(4, 'tt', 'se', 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `item_id` int(11) NOT NULL,
  `item_code` varchar(128) NOT NULL,
  `item_description` varchar(512) NOT NULL,
  `item_sub_description` varchar(512) NOT NULL,
  `item_serial` varchar(128) NOT NULL,
  `date_received` varchar(128) NOT NULL,
  `date_purchased` varchar(128) NOT NULL,
  `warranty_to` varchar(128) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `inbundle` tinyint(4) NOT NULL DEFAULT '0',
  `bundle_id` int(11) NOT NULL,
  `item_price` double NOT NULL,
  `or_no` varchar(512) NOT NULL,
  `item_type_id` int(11) NOT NULL,
  `maker_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `remarks` varchar(1024) NOT NULL,
  `allow_borrow` tinyint(4) NOT NULL DEFAULT '0',
  `item_image` longblob NOT NULL,
  `entered_by` varchar(512) NOT NULL,
  `date_entered` varchar(128) NOT NULL,
  `inventory_by` varchar(512) NOT NULL,
  `date_last_inventory` varchar(128) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_for_repair_warranty`
--

CREATE TABLE `item_for_repair_warranty` (
  `item_id` int(11) NOT NULL,
  `inbundle` tinyint(4) NOT NULL,
  `report_details` varchar(1024) NOT NULL,
  `date_reported` varchar(128) NOT NULL,
  `reported_by` varchar(512) NOT NULL,
  `date_mark` varchar(128) NOT NULL,
  `date_expected_recieved` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_for_repair_warranty_master`
--

CREATE TABLE `item_for_repair_warranty_master` (
  `item_id` int(11) NOT NULL,
  `inbundle` tinyint(4) NOT NULL,
  `report_details` varchar(1024) NOT NULL,
  `date_reported` varchar(128) NOT NULL,
  `reported_by` varchar(512) NOT NULL,
  `date_mark` varchar(128) NOT NULL,
  `date_expected_recieved` varchar(128) NOT NULL,
  `remarks` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_history`
--

CREATE TABLE `item_history` (
  `item_id` int(11) NOT NULL,
  `item_code` varchar(128) NOT NULL,
  `item_description` varchar(512) NOT NULL,
  `item_sub_description` varchar(512) NOT NULL,
  `item_serial` varchar(128) NOT NULL,
  `date_received` varchar(128) NOT NULL,
  `date_purchased` varchar(128) NOT NULL,
  `warranty_to` varchar(128) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `inbundle` tinyint(4) NOT NULL DEFAULT '0',
  `bundle_id` int(11) NOT NULL,
  `item_price` double NOT NULL,
  `or_no` varchar(512) NOT NULL,
  `item_type_id` int(11) NOT NULL,
  `maker_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `remarks` varchar(1024) NOT NULL,
  `allow_borrow` tinyint(4) NOT NULL DEFAULT '0',
  `item_image` longblob NOT NULL,
  `entered_by` varchar(512) NOT NULL,
  `date_entered` varchar(128) NOT NULL,
  `inventory_by` varchar(512) NOT NULL,
  `date_last_inventory` varchar(128) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `new_update` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_status`
--

CREATE TABLE `item_status` (
  `id` int(11) NOT NULL,
  `status_description` varchar(512) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_status`
--

INSERT INTO `item_status` (`id`, `status_description`, `active`) VALUES
(1, 'Active', 1),
(2, 'Damage', 1),
(3, 'For Replace', 1);

-- --------------------------------------------------------

--
-- Table structure for table `item_type`
--

CREATE TABLE `item_type` (
  `id` int(11) NOT NULL,
  `type_description` varchar(512) NOT NULL,
  `type_note` varchar(1024) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_type`
--

INSERT INTO `item_type` (`id`, `type_description`, `type_note`, `active`) VALUES
(1, 'Memory', 'None', 1),
(2, 'Hard Disk', 'SSD / HDD', 1),
(3, 'Network', 'None', 1);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `location_description` varchar(512) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `location_description`, `active`) VALUES
(1, 'Room 103', 1),
(2, 'Room 102', 1),
(4, 'Room 101', 1);

-- --------------------------------------------------------

--
-- Table structure for table `maker`
--

CREATE TABLE `maker` (
  `id` int(11) NOT NULL,
  `maker_description` varchar(1024) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `maker`
--

INSERT INTO `maker` (`id`, `maker_description`, `active`) VALUES
(1, 'ASUS', 1),
(2, 'AMD', 1),
(3, 'INTEL', 1);

-- --------------------------------------------------------

--
-- Table structure for table `remove_items`
--

CREATE TABLE `remove_items` (
  `item_id` int(11) NOT NULL,
  `item_code` varchar(128) NOT NULL,
  `item_description` varchar(512) NOT NULL,
  `item_sub_description` varchar(512) NOT NULL,
  `item_serial` varchar(128) NOT NULL,
  `date_received` varchar(128) NOT NULL,
  `date_purchased` varchar(128) NOT NULL,
  `warranty_to` varchar(128) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `inbundle` tinyint(4) NOT NULL DEFAULT '0',
  `bundle_id` int(11) NOT NULL,
  `item_price` double NOT NULL,
  `or_no` varchar(512) NOT NULL,
  `item_type_id` int(11) NOT NULL,
  `maker_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `allow_borrow` tinyint(4) NOT NULL DEFAULT '0',
  `item_image` longblob NOT NULL,
  `entered_by` varchar(512) NOT NULL,
  `date_entered` varchar(128) NOT NULL,
  `inventory_by` varchar(512) NOT NULL,
  `date_last_inventory` varchar(128) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `reason` varchar(1024) NOT NULL,
  `approved_by` varchar(512) NOT NULL,
  `approved_date` varchar(128) NOT NULL,
  `remarks` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `supp_name` varchar(512) NOT NULL,
  `supp_detail` varchar(512) NOT NULL,
  `supp_note` varchar(1024) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `supp_name`, `supp_detail`, `supp_note`, `active`) VALUES
(1, 'Kenneth John S. Basilio', '09300251419', 'Testing', 1),
(2, 'Ronald Castro', 'None', 'None', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bundle`
--
ALTER TABLE `bundle`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bundle_code` (`bundle_code`),
  ADD UNIQUE KEY `bundle_description` (`bundle_description`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `item_for_repair_warranty`
--
ALTER TABLE `item_for_repair_warranty`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `item_history`
--
ALTER TABLE `item_history`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `item_status`
--
ALTER TABLE `item_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_type`
--
ALTER TABLE `item_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type_description` (`type_description`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `location_description` (`location_description`);

--
-- Indexes for table `maker`
--
ALTER TABLE `maker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `remove_items`
--
ALTER TABLE `remove_items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `supp_name` (`supp_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bundle`
--
ALTER TABLE `bundle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item_history`
--
ALTER TABLE `item_history`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item_status`
--
ALTER TABLE `item_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `item_type`
--
ALTER TABLE `item_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `maker`
--
ALTER TABLE `maker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `remove_items`
--
ALTER TABLE `remove_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
