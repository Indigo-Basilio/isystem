﻿namespace iSystem
{
    partial class dlgItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dlgItem));
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtCode = new MetroFramework.Controls.MetroTextBox();
            this.cbxType = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.cbxMaker = new MetroFramework.Controls.MetroComboBox();
            this.txtDescription = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtSubDescription = new MetroFramework.Controls.MetroTextBox();
            this.lbl = new MetroFramework.Controls.MetroLabel();
            this.cbInBundle = new MetroFramework.Controls.MetroCheckBox();
            this.cbxBundle = new MetroFramework.Controls.MetroComboBox();
            this.btnAddBundle = new System.Windows.Forms.Button();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.cbxSupplier = new MetroFramework.Controls.MetroComboBox();
            this.txtUnitPrice = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.txtOrNo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.dtDatePurchased = new MetroFramework.Controls.MetroDateTime();
            this.dtReceived = new MetroFramework.Controls.MetroDateTime();
            this.dtWarrantyDate = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.cbxLocation = new MetroFramework.Controls.MetroComboBox();
            this.txtRemarks = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.cbAllowBorrow = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.cbxStatus = new MetroFramework.Controls.MetroComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnSaveClose = new MetroFramework.Controls.MetroButton();
            this.btnSaveNew = new MetroFramework.Controls.MetroButton();
            this.btnClose = new MetroFramework.Controls.MetroButton();
            this.btnSave = new MetroFramework.Controls.MetroButton();
            this.txtSerial = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(26, 61);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(54, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Code: *";
            // 
            // txtCode
            // 
            this.txtCode.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtCode.CustomButton.BackColor = System.Drawing.Color.White;
            this.txtCode.CustomButton.Image = null;
            this.txtCode.CustomButton.Location = new System.Drawing.Point(230, 1);
            this.txtCode.CustomButton.Name = "";
            this.txtCode.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCode.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCode.CustomButton.TabIndex = 1;
            this.txtCode.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCode.CustomButton.UseSelectable = true;
            this.txtCode.CustomButton.UseVisualStyleBackColor = false;
            this.txtCode.CustomButton.Visible = false;
            this.txtCode.Lines = new string[0];
            this.txtCode.Location = new System.Drawing.Point(26, 83);
            this.txtCode.MaxLength = 32767;
            this.txtCode.Name = "txtCode";
            this.txtCode.PasswordChar = '\0';
            this.txtCode.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCode.SelectedText = "";
            this.txtCode.SelectionLength = 0;
            this.txtCode.SelectionStart = 0;
            this.txtCode.ShortcutsEnabled = true;
            this.txtCode.Size = new System.Drawing.Size(252, 23);
            this.txtCode.TabIndex = 0;
            this.txtCode.UseSelectable = true;
            this.txtCode.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCode.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // cbxType
            // 
            this.cbxType.FormattingEnabled = true;
            this.cbxType.ItemHeight = 23;
            this.cbxType.Location = new System.Drawing.Point(26, 135);
            this.cbxType.Name = "cbxType";
            this.cbxType.Size = new System.Drawing.Size(252, 29);
            this.cbxType.TabIndex = 1;
            this.cbxType.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(26, 113);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(49, 19);
            this.metroLabel2.TabIndex = 3;
            this.metroLabel2.Text = "Type: *";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(76, 182);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(60, 19);
            this.metroLabel3.TabIndex = 5;
            this.metroLabel3.Text = "Maker: *";
            // 
            // cbxMaker
            // 
            this.cbxMaker.FormattingEnabled = true;
            this.cbxMaker.ItemHeight = 23;
            this.cbxMaker.Location = new System.Drawing.Point(142, 182);
            this.cbxMaker.Name = "cbxMaker";
            this.cbxMaker.Size = new System.Drawing.Size(337, 29);
            this.cbxMaker.TabIndex = 2;
            this.cbxMaker.UseSelectable = true;
            // 
            // txtDescription
            // 
            // 
            // 
            // 
            this.txtDescription.CustomButton.Image = null;
            this.txtDescription.CustomButton.Location = new System.Drawing.Point(315, 1);
            this.txtDescription.CustomButton.Name = "";
            this.txtDescription.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtDescription.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDescription.CustomButton.TabIndex = 1;
            this.txtDescription.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDescription.CustomButton.UseSelectable = true;
            this.txtDescription.CustomButton.Visible = false;
            this.txtDescription.Lines = new string[0];
            this.txtDescription.Location = new System.Drawing.Point(142, 217);
            this.txtDescription.MaxLength = 32767;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.PasswordChar = '\0';
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDescription.SelectedText = "";
            this.txtDescription.SelectionLength = 0;
            this.txtDescription.SelectionStart = 0;
            this.txtDescription.ShortcutsEnabled = true;
            this.txtDescription.Size = new System.Drawing.Size(337, 23);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.UseSelectable = true;
            this.txtDescription.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtDescription.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(49, 217);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(87, 19);
            this.metroLabel4.TabIndex = 6;
            this.metroLabel4.Text = "Description: *";
            // 
            // txtSubDescription
            // 
            // 
            // 
            // 
            this.txtSubDescription.CustomButton.Image = null;
            this.txtSubDescription.CustomButton.Location = new System.Drawing.Point(315, 1);
            this.txtSubDescription.CustomButton.Name = "";
            this.txtSubDescription.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSubDescription.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSubDescription.CustomButton.TabIndex = 1;
            this.txtSubDescription.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSubDescription.CustomButton.UseSelectable = true;
            this.txtSubDescription.CustomButton.Visible = false;
            this.txtSubDescription.Lines = new string[0];
            this.txtSubDescription.Location = new System.Drawing.Point(142, 246);
            this.txtSubDescription.MaxLength = 32767;
            this.txtSubDescription.Name = "txtSubDescription";
            this.txtSubDescription.PasswordChar = '\0';
            this.txtSubDescription.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSubDescription.SelectedText = "";
            this.txtSubDescription.SelectionLength = 0;
            this.txtSubDescription.SelectionStart = 0;
            this.txtSubDescription.ShortcutsEnabled = true;
            this.txtSubDescription.Size = new System.Drawing.Size(337, 23);
            this.txtSubDescription.TabIndex = 4;
            this.txtSubDescription.UseSelectable = true;
            this.txtSubDescription.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSubDescription.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(23, 246);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(113, 19);
            this.lbl.TabIndex = 8;
            this.lbl.Text = "Sub Description: *";
            // 
            // cbInBundle
            // 
            this.cbInBundle.AutoSize = true;
            this.cbInBundle.Location = new System.Drawing.Point(69, 322);
            this.cbInBundle.Name = "cbInBundle";
            this.cbInBundle.Size = new System.Drawing.Size(60, 15);
            this.cbInBundle.TabIndex = 6;
            this.cbInBundle.Text = "Bundle";
            this.cbInBundle.UseSelectable = true;
            this.cbInBundle.CheckedChanged += new System.EventHandler(this.cbInBundle_CheckedChanged);
            // 
            // cbxBundle
            // 
            this.cbxBundle.Enabled = false;
            this.cbxBundle.FormattingEnabled = true;
            this.cbxBundle.ItemHeight = 23;
            this.cbxBundle.Location = new System.Drawing.Point(142, 316);
            this.cbxBundle.Name = "cbxBundle";
            this.cbxBundle.Size = new System.Drawing.Size(277, 29);
            this.cbxBundle.TabIndex = 7;
            this.cbxBundle.UseSelectable = true;
            this.cbxBundle.SelectedIndexChanged += new System.EventHandler(this.cbxBundle_SelectedIndexChanged);
            // 
            // btnAddBundle
            // 
            this.btnAddBundle.Location = new System.Drawing.Point(437, 316);
            this.btnAddBundle.Name = "btnAddBundle";
            this.btnAddBundle.Size = new System.Drawing.Size(42, 29);
            this.btnAddBundle.TabIndex = 8;
            this.btnAddBundle.Text = "+";
            this.btnAddBundle.UseVisualStyleBackColor = true;
            this.btnAddBundle.Click += new System.EventHandler(this.btnAddBundle_Click);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(23, 351);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(71, 19);
            this.metroLabel6.TabIndex = 14;
            this.metroLabel6.Text = "Supplier: *";
            // 
            // cbxSupplier
            // 
            this.cbxSupplier.FormattingEnabled = true;
            this.cbxSupplier.ItemHeight = 23;
            this.cbxSupplier.Location = new System.Drawing.Point(95, 351);
            this.cbxSupplier.Name = "cbxSupplier";
            this.cbxSupplier.Size = new System.Drawing.Size(384, 29);
            this.cbxSupplier.TabIndex = 9;
            this.cbxSupplier.UseSelectable = true;
            // 
            // txtUnitPrice
            // 
            // 
            // 
            // 
            this.txtUnitPrice.CustomButton.Image = null;
            this.txtUnitPrice.CustomButton.Location = new System.Drawing.Point(111, 1);
            this.txtUnitPrice.CustomButton.Name = "";
            this.txtUnitPrice.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtUnitPrice.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtUnitPrice.CustomButton.TabIndex = 1;
            this.txtUnitPrice.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtUnitPrice.CustomButton.UseSelectable = true;
            this.txtUnitPrice.CustomButton.Visible = false;
            this.txtUnitPrice.Lines = new string[0];
            this.txtUnitPrice.Location = new System.Drawing.Point(107, 386);
            this.txtUnitPrice.MaxLength = 32767;
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.PasswordChar = '\0';
            this.txtUnitPrice.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUnitPrice.SelectedText = "";
            this.txtUnitPrice.SelectionLength = 0;
            this.txtUnitPrice.SelectionStart = 0;
            this.txtUnitPrice.ShortcutsEnabled = true;
            this.txtUnitPrice.Size = new System.Drawing.Size(133, 23);
            this.txtUnitPrice.TabIndex = 10;
            this.txtUnitPrice.UseSelectable = true;
            this.txtUnitPrice.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtUnitPrice.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(23, 386);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(78, 19);
            this.metroLabel7.TabIndex = 15;
            this.metroLabel7.Text = "Unit Price: *";
            // 
            // txtOrNo
            // 
            // 
            // 
            // 
            this.txtOrNo.CustomButton.Image = null;
            this.txtOrNo.CustomButton.Location = new System.Drawing.Point(135, 1);
            this.txtOrNo.CustomButton.Name = "";
            this.txtOrNo.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtOrNo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtOrNo.CustomButton.TabIndex = 1;
            this.txtOrNo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtOrNo.CustomButton.UseSelectable = true;
            this.txtOrNo.CustomButton.Visible = false;
            this.txtOrNo.Lines = new string[0];
            this.txtOrNo.Location = new System.Drawing.Point(323, 386);
            this.txtOrNo.MaxLength = 32767;
            this.txtOrNo.Name = "txtOrNo";
            this.txtOrNo.PasswordChar = '\0';
            this.txtOrNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtOrNo.SelectedText = "";
            this.txtOrNo.SelectionLength = 0;
            this.txtOrNo.SelectionStart = 0;
            this.txtOrNo.ShortcutsEnabled = true;
            this.txtOrNo.Size = new System.Drawing.Size(157, 23);
            this.txtOrNo.TabIndex = 11;
            this.txtOrNo.UseSelectable = true;
            this.txtOrNo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtOrNo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(258, 386);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(63, 19);
            this.metroLabel8.TabIndex = 17;
            this.metroLabel8.Text = "OR No: *";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(16, 415);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(113, 19);
            this.metroLabel9.TabIndex = 19;
            this.metroLabel9.Text = "Date Purchased: *";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(265, 415);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(105, 19);
            this.metroLabel10.TabIndex = 21;
            this.metroLabel10.Text = "Date Received: *";
            // 
            // dtDatePurchased
            // 
            this.dtDatePurchased.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDatePurchased.Location = new System.Drawing.Point(131, 415);
            this.dtDatePurchased.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtDatePurchased.Name = "dtDatePurchased";
            this.dtDatePurchased.Size = new System.Drawing.Size(109, 29);
            this.dtDatePurchased.TabIndex = 12;
            // 
            // dtReceived
            // 
            this.dtReceived.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtReceived.Location = new System.Drawing.Point(376, 415);
            this.dtReceived.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtReceived.Name = "dtReceived";
            this.dtReceived.Size = new System.Drawing.Size(103, 29);
            this.dtReceived.TabIndex = 13;
            // 
            // dtWarrantyDate
            // 
            this.dtWarrantyDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtWarrantyDate.Location = new System.Drawing.Point(131, 450);
            this.dtWarrantyDate.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtWarrantyDate.Name = "dtWarrantyDate";
            this.dtWarrantyDate.Size = new System.Drawing.Size(109, 29);
            this.dtWarrantyDate.TabIndex = 14;
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(23, 450);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(106, 19);
            this.metroLabel11.TabIndex = 24;
            this.metroLabel11.Text = "Warranty Date: *";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(250, 450);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(71, 19);
            this.metroLabel12.TabIndex = 27;
            this.metroLabel12.Text = "Location: *";
            // 
            // cbxLocation
            // 
            this.cbxLocation.FormattingEnabled = true;
            this.cbxLocation.ItemHeight = 23;
            this.cbxLocation.Location = new System.Drawing.Point(327, 450);
            this.cbxLocation.Name = "cbxLocation";
            this.cbxLocation.Size = new System.Drawing.Size(153, 29);
            this.cbxLocation.TabIndex = 15;
            this.cbxLocation.UseSelectable = true;
            // 
            // txtRemarks
            // 
            // 
            // 
            // 
            this.txtRemarks.CustomButton.Image = null;
            this.txtRemarks.CustomButton.Location = new System.Drawing.Point(342, 2);
            this.txtRemarks.CustomButton.Name = "";
            this.txtRemarks.CustomButton.Size = new System.Drawing.Size(39, 39);
            this.txtRemarks.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtRemarks.CustomButton.TabIndex = 1;
            this.txtRemarks.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtRemarks.CustomButton.UseSelectable = true;
            this.txtRemarks.CustomButton.Visible = false;
            this.txtRemarks.Lines = new string[0];
            this.txtRemarks.Location = new System.Drawing.Point(96, 485);
            this.txtRemarks.MaxLength = 32767;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.PasswordChar = '\0';
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtRemarks.SelectedText = "";
            this.txtRemarks.SelectionLength = 0;
            this.txtRemarks.SelectionStart = 0;
            this.txtRemarks.ShortcutsEnabled = true;
            this.txtRemarks.Size = new System.Drawing.Size(384, 44);
            this.txtRemarks.TabIndex = 16;
            this.txtRemarks.UseSelectable = true;
            this.txtRemarks.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtRemarks.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(28, 485);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(62, 19);
            this.metroLabel13.TabIndex = 28;
            this.metroLabel13.Text = "Remarks:";
            // 
            // cbAllowBorrow
            // 
            this.cbAllowBorrow.AutoSize = true;
            this.cbAllowBorrow.Location = new System.Drawing.Point(351, 535);
            this.cbAllowBorrow.Name = "cbAllowBorrow";
            this.cbAllowBorrow.Size = new System.Drawing.Size(129, 15);
            this.cbAllowBorrow.TabIndex = 18;
            this.cbAllowBorrow.Text = "Allow for Borrowing";
            this.cbAllowBorrow.UseSelectable = true;
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(34, 535);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(56, 19);
            this.metroLabel14.TabIndex = 32;
            this.metroLabel14.Text = "Status: *";
            // 
            // cbxStatus
            // 
            this.cbxStatus.FormattingEnabled = true;
            this.cbxStatus.ItemHeight = 23;
            this.cbxStatus.Location = new System.Drawing.Point(96, 535);
            this.cbxStatus.Name = "cbxStatus";
            this.cbxStatus.Size = new System.Drawing.Size(247, 29);
            this.cbxStatus.TabIndex = 17;
            this.cbxStatus.UseSelectable = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(322, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(157, 143);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 33;
            this.pictureBox1.TabStop = false;
            // 
            // btnSaveClose
            // 
            this.btnSaveClose.Location = new System.Drawing.Point(240, 589);
            this.btnSaveClose.Name = "btnSaveClose";
            this.btnSaveClose.Size = new System.Drawing.Size(103, 42);
            this.btnSaveClose.TabIndex = 21;
            this.btnSaveClose.Text = "Save and Close";
            this.btnSaveClose.UseSelectable = true;
            this.btnSaveClose.Click += new System.EventHandler(this.btnSaveClose_Click);
            // 
            // btnSaveNew
            // 
            this.btnSaveNew.Location = new System.Drawing.Point(131, 589);
            this.btnSaveNew.Name = "btnSaveNew";
            this.btnSaveNew.Size = new System.Drawing.Size(103, 42);
            this.btnSaveNew.TabIndex = 20;
            this.btnSaveNew.Text = "Save and New";
            this.btnSaveNew.UseSelectable = true;
            this.btnSaveNew.Click += new System.EventHandler(this.btnSaveNew_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(376, 589);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(103, 42);
            this.btnClose.TabIndex = 22;
            this.btnClose.Text = "Close";
            this.btnClose.UseSelectable = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(22, 589);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(103, 42);
            this.btnSave.TabIndex = 19;
            this.btnSave.Text = "Save";
            this.btnSave.UseSelectable = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtSerial
            // 
            // 
            // 
            // 
            this.txtSerial.CustomButton.Image = null;
            this.txtSerial.CustomButton.Location = new System.Drawing.Point(315, 1);
            this.txtSerial.CustomButton.Name = "";
            this.txtSerial.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSerial.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSerial.CustomButton.TabIndex = 1;
            this.txtSerial.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSerial.CustomButton.UseSelectable = true;
            this.txtSerial.CustomButton.Visible = false;
            this.txtSerial.Lines = new string[0];
            this.txtSerial.Location = new System.Drawing.Point(142, 275);
            this.txtSerial.MaxLength = 32767;
            this.txtSerial.Name = "txtSerial";
            this.txtSerial.PasswordChar = '\0';
            this.txtSerial.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSerial.SelectedText = "";
            this.txtSerial.SelectionLength = 0;
            this.txtSerial.SelectionStart = 0;
            this.txtSerial.ShortcutsEnabled = true;
            this.txtSerial.Size = new System.Drawing.Size(337, 23);
            this.txtSerial.TabIndex = 5;
            this.txtSerial.UseSelectable = true;
            this.txtSerial.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSerial.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(28, 275);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(108, 19);
            this.metroLabel5.TabIndex = 38;
            this.metroLabel5.Text = "Serial Number: *";
            // 
            // dlgItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 645);
            this.Controls.Add(this.txtSerial);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.btnSaveClose);
            this.Controls.Add(this.btnSaveNew);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.metroLabel14);
            this.Controls.Add(this.cbxStatus);
            this.Controls.Add(this.cbAllowBorrow);
            this.Controls.Add(this.txtRemarks);
            this.Controls.Add(this.metroLabel13);
            this.Controls.Add(this.metroLabel12);
            this.Controls.Add(this.cbxLocation);
            this.Controls.Add(this.dtWarrantyDate);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.dtReceived);
            this.Controls.Add(this.dtDatePurchased);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.txtOrNo);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.txtUnitPrice);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.cbxSupplier);
            this.Controls.Add(this.btnAddBundle);
            this.Controls.Add(this.cbxBundle);
            this.Controls.Add(this.cbInBundle);
            this.Controls.Add(this.txtSubDescription);
            this.Controls.Add(this.lbl);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.cbxMaker);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.cbxType);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.metroLabel1);
            this.MaximizeBox = false;
            this.Name = "dlgItem";
            this.Text = "Add / Update [Item]";
            this.Load += new System.EventHandler(this.dlgItem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtCode;
        private MetroFramework.Controls.MetroComboBox cbxType;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroComboBox cbxMaker;
        private MetroFramework.Controls.MetroTextBox txtDescription;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtSubDescription;
        private MetroFramework.Controls.MetroLabel lbl;
        private MetroFramework.Controls.MetroCheckBox cbInBundle;
        private MetroFramework.Controls.MetroComboBox cbxBundle;
        private System.Windows.Forms.Button btnAddBundle;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroComboBox cbxSupplier;
        private MetroFramework.Controls.MetroTextBox txtUnitPrice;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox txtOrNo;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroDateTime dtDatePurchased;
        private MetroFramework.Controls.MetroDateTime dtReceived;
        private MetroFramework.Controls.MetroDateTime dtWarrantyDate;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroComboBox cbxLocation;
        private MetroFramework.Controls.MetroTextBox txtRemarks;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroCheckBox cbAllowBorrow;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroComboBox cbxStatus;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroButton btnSaveClose;
        private MetroFramework.Controls.MetroButton btnSaveNew;
        private MetroFramework.Controls.MetroButton btnClose;
        private MetroFramework.Controls.MetroButton btnSave;
        private MetroFramework.Controls.MetroTextBox txtSerial;
        private MetroFramework.Controls.MetroLabel metroLabel5;
    }
}