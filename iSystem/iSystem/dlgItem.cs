﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class dlgItem : MetroFramework.Forms.MetroForm
    {
        public dlgItem()
        {
            InitializeComponent();
        }
        ItemType ItemType = new ItemType();
        Maker Maker = new Maker();
        Bundle Bundle = new Bundle();
        Supplier Supplier = new Supplier();
        Location Location = new Location();
        ItemStatus ItemStatus = new ItemStatus();
        ItemSTI ItemSTI = new ItemSTI();
        List<ItemType> ItemTypes = new List<ItemType>();
        List<Maker> Makers = new List<Maker>();
        List<Bundle> Bundles = new List<Bundle>();
        List<Supplier> Suppliers = new List<Supplier>();
        List<Location> Locations = new List<Location>();
        List<ItemStatus> ItemStatuss = new List<ItemStatus>();
        List<ItemSTI> ItemSTIs = new List<ItemSTI>();
        string action;
        bool update = false;
        private void renderItemType()
        {
            ItemTypes.Clear();
            cbxType.Items.Clear();
            ItemType.Active = 1;
            ItemTypes = ItemType.Read();
            foreach (var item in ItemTypes)
            {
                cbxType.Items.Add(item.Description);
            }
        }
        private void renderMaker()
        {
            Makers.Clear();
            cbxMaker.Items.Clear();
            Maker.Active = 1;
            Makers = Maker.Read();
            foreach (var item in Makers)
            {
                cbxMaker.Items.Add(item.Description);
            }
        }
        private void renderBundle()
        {
            Bundles.Clear();
            cbxBundle.Items.Clear();
            Maker.Active = 1;
            Bundles = Bundle.Read();
            foreach (var item in Bundles)
            {
                cbxBundle.Items.Add(item.Code);
            }
        }
        private void renderSupplier()
        {
            Suppliers.Clear();
            cbxSupplier.Items.Clear();
            Supplier.Active = 1;
            Suppliers = Supplier.Read();
            foreach (var item in Suppliers)
            {
                cbxSupplier.Items.Add(item.Name);
            }
        }
        private void renderLocation()
        {
            Locations.Clear();
            cbxLocation.Items.Clear();
            Location.Active = 1;
            Locations = Location.Read();
            foreach (var item in Locations)
            {
                cbxLocation.Items.Add(item.Description);
            }
        }
        private void renderStatus()
        {
            ItemStatuss.Clear();
            cbxStatus.Items.Clear();
            ItemStatus.Active = 1;
            ItemStatuss = ItemStatus.Read();
            foreach (var item in ItemStatuss)
            {
                cbxStatus.Items.Add(item.Description);
            }
            //cbxStatus.Text = "Active";
        }
        private void dlgItem_Load(object sender, EventArgs e)
        {
            this.renderItemType();
            this.renderMaker();
            this.renderSupplier();
            this.renderLocation();
            this.renderStatus();
        }

        private void cbInBundle_CheckedChanged(object sender, EventArgs e)
        {
            if (cbInBundle.Checked)
            {
                this.renderBundle();
                cbxBundle.Enabled = true;
            }
            else
            {
                cbxBundle.Items.Clear();
                cbxBundle.Enabled = false;
            }
        }
        private void Save()
        {
            ValidateLibrary validate = new ValidateLibrary();
            validate.Required(txtCode);
            validate.Required(cbxType);
            validate.Required(cbxMaker);
            validate.Required(txtDescription);
            validate.Required(lbl);
            validate.Required(txtSerial);
            validate.Required(cbxSupplier);
            validate.Rule(txtUnitPrice, false, "double");
            validate.Required(cbxLocation);
            validate.Required(cbxStatus);

            if (validate.WithError())
            {
                Messages.Instance.Error("Please correct all required fields.");
                return;
            }

            ItemSTI.Code = txtCode.Text;

            ItemType.Description = cbxType.Text;
            ItemSTI.ItemTypeId = ItemType.GetByName();

            Maker.Description = cbxMaker.Text;
            ItemSTI.MakerId = Maker.GetByName();

            ItemSTI.Description = txtDescription.Text;
            ItemSTI.SubDescription = lbl.Text;
            ItemSTI.Serial = txtSerial.Text;
            if (cbInBundle.Checked)
            {
                ItemSTI.Inbundle = 1;
                Bundle.Description = cbxBundle.Text;
                ItemSTI.BundleId = Bundle.GetByName();
                ItemSTI.LocationId = Bundle.GetByNameAgain();//Get Location Id
            }
            else
            {
                ItemSTI.Inbundle = 0;
                ItemSTI.BundleId = 1; //NONE in database
                Location.Description = cbxLocation.Text;
                ItemSTI.LocationId = Location.GetByName();
            }

            Supplier.Name = cbxSupplier.Text;
            ItemSTI.SupplierId = Supplier.GetByName();

            if(txtUnitPrice.Text != "")
            {
                ItemSTI.Price = double.Parse(txtUnitPrice.Text);
            }
            else
            {
                ItemSTI.Price = 0.00;
            }

            if(txtOrNo.Text != "")
            {
                ItemSTI.OrNumber = txtOrNo.Text;
            }
            else
            {
                ItemSTI.OrNumber = "0";
            }
            
            ItemSTI.DatePurchased = DateLibrary.Instance.ConvertDate(dtDatePurchased.Text, "yyyy-MM-dd");
            ItemSTI.DateReceived = DateLibrary.Instance.ConvertDate(dtReceived.Text, "yyyy-MM-dd");
            ItemSTI.WarrantyTo = DateLibrary.Instance.ConvertDate(dtWarrantyDate.Text, "yyyy-MM-dd");

           

            ItemSTI.Remarks = txtRemarks.Text;

            if (cbAllowBorrow.Checked)
            {
                ItemSTI.AllowBorrow = 1;
            }
            else
            {
                ItemSTI.AllowBorrow = 0;
            }

            ItemStatus.Description = cbxStatus.Text;
            
            ItemSTI.StatusId = ItemStatus.GetByName();

            ItemSTI.Image = ImageLibrary.Instance.imageToByteArray(pictureBox1.Image);

            ItemSTI.EnteredBy = UniversalUse.ThisPosition + "[" + UniversalUse.ThisPosition + "]";
            ItemSTI.InventoryBy = UniversalUse.ThisPosition + "[" + UniversalUse.ThisPosition + "]";
            ItemSTI.GetDateAndTimeDatabase();


            if (update)
            {
                ItemSTIs.Clear();
                ItemSTIs = ItemSTI.GetById();
                ItemSTI.Update();
            }
            else
            {
               
                ItemSTI.Save();
            }
            if (action == "Save")
            {
                update = true;
            }
            else if (action == "Save and Close")
            {
                this.Close();
                this.Dispose();
            }
            else if (action == "Save and New")
            {
                this.ResetForm();
            }
        }
        private void ResetForm()
        {
            Helper helper = new Helper();
            helper.Clear(this);

            update = false;
        }
        private void btnAddBundle_Click(object sender, EventArgs e)
        {
            dlgBundle dlgBundle = new dlgBundle();
            dlgBundle.ShowDialog();
            this.renderBundle();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            action = "Save";
            this.Save();
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            action = "Save and New";
            this.Save();
        }

        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            action = "Save and Close";
            this.Save();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void cbxBundle_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bundle.Code = cbxBundle.Text; //Parameter
            MessageBox.Show(Bundle.Code);
            Bundle.GetByName(); //Return int
            MessageBox.Show(Bundle.Id.ToString());
            Bundle.GetById(); //Return string
            cbxLocation.Text = Bundle.LocationDescription;
            MessageBox.Show(Bundle.LocationDescription);
        }
    }
}
