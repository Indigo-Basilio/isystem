﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iSystem
{
    public static class UniversalUse
    {
        public static string UserId { get; set; }
        public static string ThisName { get; set; }
        public static string ThisPosition { get; set; }
        public static int ThisId { get; set; }

        public static bool ThisConnect { get; set; }
    }
}
