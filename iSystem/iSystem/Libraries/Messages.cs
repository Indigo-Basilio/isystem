﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using MetroFramework.Forms;

namespace iSystem
{
    class Messages 
    {
                /* ************************************************
        * Private Properties
        * ***********************************************/
        private static Messages instance = null;

        private static readonly object padlock = new object();

        /*************************************************
        * Protected Properties
        * ***********************************************/

        /*************************************************
         * Public Properties
         * ***********************************************/

        /* ***********************************************
         * Constructor
         * ***********************************************/
        Messages()
        {
        }

        public static string LabelMessage { get; set; }

        public static int score { get; set; }

        /*************************************************
         * Private Methods
         * ***********************************************/

        /*************************************************
         * Protected Methods
         * ************************************************/

        /*************************************************
         * Public Methods
         * ************************************************/

        /// <summary>
        /// Create a one time instance for this library.
        /// Avoiding multiple instances when called.
        /// </summary>
        public static Messages Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Messages();
                    }
                    return instance;
                }
            }
        }

        /// <summary>
        /// Default error message
        /// </summary>
        /// <param name="message">string</param>
        public void Error(string message)
        {           

            MessageBox.Show(message, "System Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Default information message
        /// </summary>
        /// <param name="message">string</param>
        public void Information(string message)
        {
            MessageBox.Show(message, "System Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Default incomplete form message
        /// </summary>
        /// <param name="message">string</param>
        public void Asterisk(string message)
        {
            MessageBox.Show(message, "System Message", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        /// <summary>
        /// Default exclamation message
        /// </summary>
        /// <param name="message">string</param>
        public void Exclamation(string message)
        {
            MessageBox.Show(message, "System Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
    }
}
