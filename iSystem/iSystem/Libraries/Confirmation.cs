﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace iSystem
{
    class Confirmation
    {
        /// <summary>
        /// message box default for Delete
        /// </summary>
        /// <param name="selected">string</param>
        /// <returns>bool</returns>
        public static bool Delete(string selected)
        {
            DialogResult dlgResult = MessageBox.Show("Delete " + selected + " record ?", "System Message", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dlgResult == DialogResult.Yes)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// message box default for save
        /// </summary>
        /// <param name="message">string</param>
        /// <returns>bool</returns>
        public static bool Save(string message)
        {
            DialogResult dlgResult = MessageBox.Show(message.ToString(), "System Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlgResult == DialogResult.Yes)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// message box default for question
        /// </summary>
        /// <param name="message">string</param>
        /// <returns>bool</returns>
        public static bool Question(string message)
        {
            

            return false;
        }

        /// <summary>
        /// message box for question with cancel
        /// </summary>
        /// <param name="message">string</param>
        /// <returns>bool</returns>
        public static string QuestionWithCancel(string message)
        {
            DialogResult dlgResult = MessageBox.Show(message.ToString(), "System Message", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            return dlgResult.ToString();
        }

        /// <summary>
        /// message box default for asterisk or warning
        /// </summary>
        /// <param name="message">string</param>
        /// <returns>bool</returns>
        public static bool Asterisk(string message)
        {
            DialogResult dlgResult = MessageBox.Show(message.ToString(), "System Message", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
            if (dlgResult == DialogResult.Yes)
            {
                return true;
            }

            return false;
        }
    }
}
