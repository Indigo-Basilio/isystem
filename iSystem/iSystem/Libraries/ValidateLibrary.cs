﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace iSystem
{
    class ValidateLibrary
    {
        /* ************************************************
         * Private Properties
         * ***********************************************/

        /*************************************************
         * Protected Properties
         * ***********************************************/

        /*************************************************
         * Public Properties
         * ***********************************************/
        public ArrayList error = new ArrayList();

        /*************************************************
         * Private Methods
         * ***********************************************/

        /*************************************************
         * Protected Methods
         * ************************************************/

        /*************************************************
         * Public Methods
         * ************************************************/

        /// <summary>
        /// Required field
        /// </summary>
        /// <param name="control"></param>
        public void Required(Control control)
        {
            if (control.Text == "")
            {
                control.BackColor = Color.MistyRose;
                error.Add(control.Name);

                return;
            }


            control.BackColor = Color.White;
        }

        /// <summary>
        /// Multiple ruling. check if required, for data type and for length.
        /// </summary>
        /// <param name="control">Control Name</param>
        /// <param name="required">true or false </param>
        /// <param name="dataType">int, float, double</param>
        /// <param name="length">length of text</param>
        public void Rule(Control control, bool required = false, string dataType = "", int length = 0)
        {
            //for required
            if (required)
            {
                if (control.Text == String.Empty)
                {
                    control.BackColor = Color.MistyRose;
                    error.Add(control.Name);

                    return;
                }
                else
                {
                    control.BackColor = Color.White;
                }
            }

            int i;
            float fl;
            double d;

            //for int
            if (dataType == "integer")
            {
                if (int.TryParse(control.Text, out i))
                {
                    if (i.GetType() != typeof(int))
                    {
                        control.BackColor = Color.MistyRose;
                        error.Add(control.Name);

                        return;
                    }
                    else
                    {
                        control.BackColor = Color.White;
                    }
                }
                else
                {
                    control.BackColor = Color.MistyRose;
                    error.Add(control.Name);

                    return;
                }
            }

            //for float
            if (dataType == "float")
            {
                if (float.TryParse(control.Text, out fl))
                {
                    if (fl.GetType() != typeof(float))
                    {
                        control.BackColor = Color.MistyRose;
                        error.Add(control.Name);

                        return;
                    }
                    else
                    {
                        control.BackColor = Color.White;
                    }

                }
                else
                {
                    control.BackColor = Color.MistyRose;
                    error.Add(control.Name);

                    return;
                }
            }

            //for double
            if (dataType == "double")
            {
                if (double.TryParse(control.Text, out d))
                {
                    if (d.GetType() != typeof(double))
                    {
                        control.BackColor = Color.MistyRose;
                        error.Add(control.Name);

                        return;
                    }
                    else
                    {
                        control.BackColor = Color.White;
                    }

                }
                else
                {
                    control.BackColor = Color.MistyRose;
                    error.Add(control.Name);

                    return;
                }
            }

            //for length
            if (control.Text.Length < length && length != 0)
            {
                control.BackColor = Color.MistyRose;
                error.Add(control.Name);

                return;
            }
            else
            {
                control.BackColor = Color.White;
            }

        }

        /// <summary>
        /// Digit only in field
        /// </summary>
        /// <param name="control"></param>
        public void DigitOnly(Control control)
        {
            double d;

            if (double.TryParse(control.Text, out d))
            {
                if (d.GetType() != typeof(double))
                {
                    control.BackColor = Color.MistyRose;
                    error.Add(control.Name);

                    return;
                }
            }
            else
            {
                control.BackColor = Color.White;
            }
        }

        /// <summary>
        /// Integer only in field
        /// </summary>
        /// <param name="control"></param>
        public void IntOnly(Control control)
        {
            int i;

            if (int.TryParse(control.Text, out i))
            {
                if (i.GetType() != typeof(int))
                {
                    control.BackColor = Color.MistyRose;
                    error.Add(control.Name);

                    return;
                }
            }
            else
            {
                control.BackColor = Color.White;
            }
        }

        /// <summary>
        /// Check if theres an error in inputted field
        /// </summary>
        /// <returns>true if error occured</returns>
       
        
        public bool WithError()
        {
            if (error.Count != 0)
            {
                return true;
            }

            return false;
        }
    }
}
