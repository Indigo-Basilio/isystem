﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace iSystem
{
    class Helper
    {
        /* ************************************************
         * Private Properties
         * ***********************************************/

        /*************************************************
         * Protected Properties
         * ***********************************************/

        /*************************************************
         * Public Properties
         * ***********************************************/


        /*************************************************
         * Private Methods
         * ***********************************************/
        /*************************************************
         * Protected Methods
         * ************************************************/

        /*************************************************
         * Public Methods
         * ************************************************/

        /// <summary>
        /// Clear textbox on form
        /// </summary>
        public void Clear(Control parent)
        {
            foreach (Control child in parent.Controls)
            {
                TextBox textBox = child as TextBox;
                RichTextBox richTextBox = child as RichTextBox;

                //clear if textbox
                if (textBox == null)
                {
                    Clear(child);
                }
                else
                {
                    textBox.Clear();                   
                }            

                //clear if richtextbox aka multiline textbox
                if(richTextBox == null)
                {
                    Clear(child);
                }
                else
                {
                    richTextBox.Clear();
                }
            }
        }

    }
}
