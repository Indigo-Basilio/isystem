﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class dlgItemType : MetroFramework.Forms.MetroForm
    {
        public dlgItemType()
        {
            InitializeComponent();
        }
        ItemType ItemType = new ItemType();
        List<ItemType> ItemTypes = new List<ItemType>();
        string action;
        bool update = false;
        private void dlgItemType_Load(object sender, EventArgs e)
        {

        }
        public void Edit(int id)
        {
            ItemTypes.Clear();
            ItemType.Id = id;
            ItemTypes = ItemType.GetById();
            txtDesc.Text = ItemTypes[0].Description;
            txtNote.Text = ItemTypes[0].Notes;
            update = true;
        }
        private void Save()
        {
            ValidateLibrary validate = new ValidateLibrary();
            validate.Required(txtDesc);
            validate.Required(txtNote);
            if (validate.WithError())
            {
                Messages.Instance.Error("Please correct all required fields.");
                return;
            }
            ItemType.Description = txtDesc.Text;
            ItemType.Notes = txtNote.Text;
            if (update)
            {
                ItemTypes.Clear();
                ItemTypes = ItemType.GetById();
                ItemType.Update();
            }
            else
            {
                ItemType.Save();
            }
            if (action == "Save")
            {               
                update = true;
            }
            else if (action == "Save and Close")
            {
                this.Close();
                this.Dispose();
            }
            else if (action == "Save and New")
            {
                this.ResetForm();
            }
        }
        private void ResetForm()
        {
            Helper helper = new Helper();
            helper.Clear(this);

            update = false;
        }
    
        private void btnSave_Click(object sender, EventArgs e)
        {
            action = "Save";
            this.Save();
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            action = "Save and New";
            this.Save();
        }

        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            action = "Save and Close";
            this.Save();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
