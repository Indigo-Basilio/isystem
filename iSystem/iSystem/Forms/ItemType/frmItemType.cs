﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class frmItemType : MetroFramework.Forms.MetroForm
    {
        public frmItemType()
        {
            InitializeComponent();
        }
        ItemType ItemType = new ItemType();
        List<ItemType> ItemTypes = new List<ItemType>();
        private void Render()
        {
            ItemTypes.Clear();
            lv.Items.Clear();
            txtSearch.Text = string.Empty;
            txtSearch.Focus();
            ItemType.Active = 1;
            ItemTypes = ItemType.Read();
            foreach (var item in ItemTypes)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Description);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Notes);
            }
        }
        private void Search()
        {
            ItemTypes.Clear();
            lv.Items.Clear();
            ItemType.Active = 1;
            ItemType.Description = txtSearch.Text;
            ItemTypes = ItemType.Search();
            foreach (var item in ItemTypes)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Description);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Notes);
            }
        }
        private void frmItemType_Load(object sender, EventArgs e)
        {
            this.Render();            
        }
        private void btnNew_Click(object sender, EventArgs e)
        {
            dlgItemType dlgItemType = new dlgItemType();
            dlgItemType.ShowDialog();
            this.Render();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if(lv.SelectedItems.Count > 0)
            {
                dlgItemType dlgItemType = new dlgItemType();
                dlgItemType.Edit(int.Parse(lv.SelectedItems[0].Text));
                dlgItemType.ShowDialog();
                this.Render();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if(Confirmation.Delete(lv.SelectedItems[0].SubItems[1].Text))
            {
                ItemType.Id = int.Parse(lv.SelectedItems[0].Text);
                ItemType.Active = 0;
                ItemType.Delete();
                this.Render();
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.Search();
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
