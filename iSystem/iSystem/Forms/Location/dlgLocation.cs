﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class dlgLocation : MetroFramework.Forms.MetroForm
    {
        public dlgLocation()
        {
            InitializeComponent();
        }
        Location location = new Location();
        List<Location> locations = new List<Location>();
        string action;
        bool update = false;
        private void dlgLocation_Load(object sender, EventArgs e)
        {

        }
        public void Edit(int id)
        {
            locations.Clear();
            location.Id = id;
            locations = location.GetById();
            txtDesc.Text = locations[0].Description;            
            update = true;
        }
        private void Save()
        {
            ValidateLibrary validate = new ValidateLibrary();
            validate.Required(txtDesc);        
            if (validate.WithError())
            {
                Messages.Instance.Error("Please correct all required fields.");
                return;
            }
            location.Description = txtDesc.Text;            
            if (update)
            {
                locations.Clear();
                locations = location.GetById();
                location.Update();
            }
            else
            {
                location.Save();
            }
            if (action == "Save")
            {
                update = true;
            }
            else if (action == "Save and Close")
            {
                this.Close();
                this.Dispose();
            }
            else if (action == "Save and New")
            {
                this.ResetForm();
            }
        }
        private void ResetForm()
        {
            Helper helper = new Helper();
            helper.Clear(this);

            update = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            action = "Save";
            this.Save();
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            action = "Save and New";
            this.Save();
        }

        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            action = "Save and Close";
            this.Save();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
