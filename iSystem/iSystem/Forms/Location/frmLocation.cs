﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class frmLocation : MetroFramework.Forms.MetroForm
    {
        public frmLocation()
        {
            InitializeComponent();
        }
        Location location = new Location();
        List<Location> locations = new List<Location>();
        private void Render()
        {
            locations.Clear();
            lv.Items.Clear();
            txtSearch.Text = string.Empty;
            txtSearch.Focus();
            location.Active = 1;
            locations = location.Read();
            foreach (var item in locations)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Description);
            }
        }
        private void Search()
        {
            locations.Clear();
            lv.Items.Clear();
            location.Active = 1;
            location.Description = txtSearch.Text;
            locations = location.Search();
            foreach (var item in locations)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Description);
            }
        }
        private void frmLocation_Load(object sender, EventArgs e)
        {
            this.Render();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            dlgLocation dlgLocation = new dlgLocation();
            dlgLocation.ShowDialog();
            this.Render();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count > 0)
            {
                dlgLocation dlgLocation = new dlgLocation();
                dlgLocation.Edit(int.Parse(lv.SelectedItems[0].Text));
                dlgLocation.ShowDialog();
                this.Render();
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Confirmation.Delete(lv.SelectedItems[0].SubItems[1].Text))
            {
                location.Id = int.Parse(lv.SelectedItems[0].Text);
                location.Active = 0;
                location.Delete();
                this.Render();
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.Search();
        }
    }
}
