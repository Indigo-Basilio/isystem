﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class MainForm : MetroFramework.Forms.MetroForm
    {
        public MainForm()
        {
            InitializeComponent();
        }

        public void CheckUser(string position)
        {
           
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.StyleManager = metroStyleManager1;
            lblUser.Text = UniversalUse.ThisName + "[" + UniversalUse.ThisPosition + "] is ONLINE.";
        }

        private void btnFileMaintenance_Click(object sender, EventArgs e)
        {
            metroContextMenu1.Show(btnFileMaintenance, 0, btnFileMaintenance.Height);
        }

        private void itemTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmItemType frmItemType = new frmItemType();
            frmItemType.ShowDialog();
        }

        private void locationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLocation frmLocation = new frmLocation();
            frmLocation.ShowDialog();
        }

        private void supplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSupplier frmSupplier = new frmSupplier();
            frmSupplier.ShowDialog();
        }

        private void itemStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmItemStatus frmItemStatus = new frmItemStatus();
            frmItemStatus.ShowDialog();
        }

        private void makerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMaker frmMaker = new frmMaker();
            frmMaker.ShowDialog();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = "System Time: " + DateTime.Now.ToLongTimeString();
            lblDate.Text = "System Time: " + DateTime.Now.ToLongDateString();
        }

        private void cbTheme_TextChanged(object sender, EventArgs e)
        {
            if(cbTheme.Text == "Light")
            {
                metroStyleManager1.Theme = MetroFramework.MetroThemeStyle.Light;
            }
            else
            {
                metroStyleManager1.Theme = MetroFramework.MetroThemeStyle.Dark;
            }
        }

        private void bundleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBundle frmBundle = new frmBundle();
            frmBundle.ShowDialog();
        }

        private void configurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConfiguration frmConfiguration = new frmConfiguration();
            frmConfiguration.ShowDialog();
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            metroContextMenu2.Show(btnSetting, 0, btnFileMaintenance.Height);
        }

        private void itemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmItem frmItem = new frmItem();
            frmItem.ShowDialog();
        }
    }
}
