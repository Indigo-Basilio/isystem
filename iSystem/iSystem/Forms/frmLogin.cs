﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class frmLogin : MetroFramework.Forms.MetroForm
    {
        public frmLogin()
        {
            InitializeComponent();
        }
        Login login = new Login();
        //AuditTrail a = new AuditTrail();
        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void frmLogin_KeyDown(object sender, KeyEventArgs e)
        {

        }       
        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        private void btLogin_Click(object sender, EventArgs e)
        {
            login.Username = txtUsername.Text;
            login.Password = HashLibrary.Encrypt(txtPassword.Text + txtUsername.Text);
            Console.WriteLine(HashLibrary.Encrypt("7.5"));
            login.DoLogin();
            if (login.Success)
            {
                MetroFramework.MetroMessageBox.Show(this, "Welcome to the system!", "System Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                UniversalUse.ThisId = login.Id;
                UniversalUse.ThisName = login.LastName + ", " + login.FirstName;
                UniversalUse.ThisPosition = login.Position;
                MainForm MainForm = new MainForm();
                MainForm.CheckUser(login.Position);
                MainForm.ShowDialog();
                this.Show();
                //frmEmployee employee = new frmEmployee();
                //employee.ShowDialog();
            }
            else if (txtPassword.Text == "Super" && txtUsername.Text == "User")
            {
                MetroFramework.MetroMessageBox.Show(this, "Welcome to the system!", "System Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                UniversalUse.ThisName = "SuperUser";
                MainForm MainForm = new MainForm();
                MainForm.CheckUser("Indigo");
                MainForm.ShowDialog();
                this.Show();
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "Invalid Username/Password.", "System Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            txtPassword.Text = string.Empty;
            txtUsername.Text = string.Empty;

        }

        private void txtUsername_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.Control && e.Alt && e.KeyCode == Keys.Home)
            {
                frmConfiguration frmConfiguration = new frmConfiguration();
                frmConfiguration.ShowDialog();
            }
            if (e.KeyCode == Keys.Enter)
            {
                btnLogin.PerformClick();
            }
        }

        private void txtPassword_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.Control && e.Alt && e.KeyCode == Keys.Home)
            {
                frmConfiguration frmConfiguration = new frmConfiguration();
                frmConfiguration.ShowDialog();
            }
            if (e.KeyCode == Keys.Enter)
            {
                btnLogin.PerformClick();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = "System Time: " + DateTime.Now.ToLongTimeString();
            lblDate.Text = "System Time: " + DateTime.Now.ToLongDateString();
        }
    }
}
