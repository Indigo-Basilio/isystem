﻿namespace iSystem
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.metroContextMenu1 = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.itemTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.locationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bundleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnFileMaintenance = new MetroFramework.Controls.MetroButton();
            this.lblUser = new MetroFramework.Controls.MetroLabel();
            this.lblTime = new MetroFramework.Controls.MetroLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblDate = new MetroFramework.Controls.MetroLabel();
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.cbTheme = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroContextMenu2 = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSetting = new MetroFramework.Controls.MetroButton();
            this.itemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.metroContextMenu1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            this.metroContextMenu2.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroContextMenu1
            // 
            this.metroContextMenu1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bundleToolStripMenuItem,
            this.itemToolStripMenuItem,
            this.itemTypeToolStripMenuItem,
            this.itemStatusToolStripMenuItem,
            this.locationToolStripMenuItem,
            this.makerToolStripMenuItem,
            this.supplierToolStripMenuItem});
            this.metroContextMenu1.Name = "metroContextMenu1";
            this.metroContextMenu1.Size = new System.Drawing.Size(134, 158);
            // 
            // itemTypeToolStripMenuItem
            // 
            this.itemTypeToolStripMenuItem.Name = "itemTypeToolStripMenuItem";
            this.itemTypeToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.itemTypeToolStripMenuItem.Text = "Item Type";
            this.itemTypeToolStripMenuItem.Click += new System.EventHandler(this.itemTypeToolStripMenuItem_Click);
            // 
            // locationToolStripMenuItem
            // 
            this.locationToolStripMenuItem.Name = "locationToolStripMenuItem";
            this.locationToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.locationToolStripMenuItem.Text = "Location";
            this.locationToolStripMenuItem.Click += new System.EventHandler(this.locationToolStripMenuItem_Click);
            // 
            // supplierToolStripMenuItem
            // 
            this.supplierToolStripMenuItem.Name = "supplierToolStripMenuItem";
            this.supplierToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.supplierToolStripMenuItem.Text = "Supplier";
            this.supplierToolStripMenuItem.Click += new System.EventHandler(this.supplierToolStripMenuItem_Click);
            // 
            // itemStatusToolStripMenuItem
            // 
            this.itemStatusToolStripMenuItem.Name = "itemStatusToolStripMenuItem";
            this.itemStatusToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.itemStatusToolStripMenuItem.Text = "Item Status";
            this.itemStatusToolStripMenuItem.Click += new System.EventHandler(this.itemStatusToolStripMenuItem_Click);
            // 
            // makerToolStripMenuItem
            // 
            this.makerToolStripMenuItem.Name = "makerToolStripMenuItem";
            this.makerToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.makerToolStripMenuItem.Text = "Maker";
            this.makerToolStripMenuItem.Click += new System.EventHandler(this.makerToolStripMenuItem_Click);
            // 
            // bundleToolStripMenuItem
            // 
            this.bundleToolStripMenuItem.Name = "bundleToolStripMenuItem";
            this.bundleToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.bundleToolStripMenuItem.Text = "Bundle";
            this.bundleToolStripMenuItem.Click += new System.EventHandler(this.bundleToolStripMenuItem_Click);
            // 
            // btnFileMaintenance
            // 
            this.btnFileMaintenance.Location = new System.Drawing.Point(24, 82);
            this.btnFileMaintenance.Name = "btnFileMaintenance";
            this.btnFileMaintenance.Size = new System.Drawing.Size(130, 33);
            this.btnFileMaintenance.TabIndex = 1;
            this.btnFileMaintenance.Text = "File Maintenance";
            this.btnFileMaintenance.UseSelectable = true;
            this.btnFileMaintenance.Click += new System.EventHandler(this.btnFileMaintenance_Click);
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblUser.Location = new System.Drawing.Point(20, 460);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(210, 19);
            this.lblUser.TabIndex = 2;
            this.lblUser.Text = "Basilio, Kenneth John S. is ONLINE.";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblTime.Location = new System.Drawing.Point(20, 441);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(41, 19);
            this.lblTime.TabIndex = 3;
            this.lblTime.Text = "Time:";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblDate.Location = new System.Drawing.Point(20, 422);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(39, 19);
            this.lblDate.TabIndex = 4;
            this.lblDate.Text = "Date:";
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = this;
            // 
            // cbTheme
            // 
            this.cbTheme.Dock = System.Windows.Forms.DockStyle.Right;
            this.cbTheme.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTheme.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbTheme.FormattingEnabled = true;
            this.cbTheme.ItemHeight = 19;
            this.cbTheme.Items.AddRange(new object[] {
            "Light",
            "Dark"});
            this.cbTheme.Location = new System.Drawing.Point(572, 60);
            this.cbTheme.Name = "cbTheme";
            this.cbTheme.Size = new System.Drawing.Size(121, 25);
            this.cbTheme.TabIndex = 6;
            this.cbTheme.UseSelectable = true;
            this.cbTheme.TextChanged += new System.EventHandler(this.cbTheme_TextChanged);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.metroLabel1.Location = new System.Drawing.Point(458, 60);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(114, 19);
            this.metroLabel1.TabIndex = 7;
            this.metroLabel1.Text = "Theme Mainform:";
            // 
            // metroContextMenu2
            // 
            this.metroContextMenu2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurationToolStripMenuItem});
            this.metroContextMenu2.Name = "metroContextMenu2";
            this.metroContextMenu2.Size = new System.Drawing.Size(149, 26);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.configurationToolStripMenuItem.Text = "Configuration";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click);
            // 
            // btnSetting
            // 
            this.btnSetting.Location = new System.Drawing.Point(160, 82);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(130, 33);
            this.btnSetting.TabIndex = 9;
            this.btnSetting.Text = "Setting";
            this.btnSetting.UseSelectable = true;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // itemToolStripMenuItem
            // 
            this.itemToolStripMenuItem.Name = "itemToolStripMenuItem";
            this.itemToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.itemToolStripMenuItem.Text = "Item";
            this.itemToolStripMenuItem.Click += new System.EventHandler(this.itemToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 499);
            this.Controls.Add(this.btnSetting);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.cbTheme);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.btnFileMaintenance);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "iSystem [Inventory System] for STI College Tarlac";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.metroContextMenu1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            this.metroContextMenu2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroContextMenu metroContextMenu1;
        private System.Windows.Forms.ToolStripMenuItem itemTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem locationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makerToolStripMenuItem;
        private MetroFramework.Controls.MetroButton btnFileMaintenance;
        private MetroFramework.Controls.MetroLabel lblUser;
        private MetroFramework.Controls.MetroLabel lblTime;
        private System.Windows.Forms.Timer timer1;
        private MetroFramework.Controls.MetroLabel lblDate;
        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroComboBox cbTheme;
        private System.Windows.Forms.ToolStripMenuItem bundleToolStripMenuItem;
        private MetroFramework.Controls.MetroContextMenu metroContextMenu2;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private MetroFramework.Controls.MetroButton btnSetting;
        private System.Windows.Forms.ToolStripMenuItem itemToolStripMenuItem;
    }
}