﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class dlgSupplier : MetroFramework.Forms.MetroForm
    {
        public dlgSupplier()
        {
            InitializeComponent();
        }
        Supplier supplier = new Supplier();
        List<Supplier> suppliers = new List<Supplier>();
        string action;
        bool update = false;
        public void Edit(int id)
        {
            suppliers.Clear();
            supplier.Id = id;
            suppliers = supplier.GetById();
            txtName.Text = suppliers[0].Name;
            txtDetail.Text = suppliers[0].Detail;
            txtNote.Text = suppliers[0].Note;
            update = true;
        }
        private void Save()
        {
            ValidateLibrary validate = new ValidateLibrary();
            validate.Required(txtName);
            validate.Required(txtDetail);
            validate.Required(txtNote);
            if (validate.WithError())
            {
                Messages.Instance.Error("Please correct all required fields.");
                return;
            }
            supplier.Name = txtName.Text;
            supplier.Detail = txtDetail.Text;
            supplier.Note = txtNote.Text;
            
            if (update)
            {
                suppliers.Clear();
                suppliers = supplier.GetById();
                supplier.Update();
            }
            else
            {
                supplier.Save();
            }
            if (action == "Save")
            {
                update = true;
            }
            else if (action == "Save and Close")
            {
                this.Close();
                this.Dispose();
            }
            else if (action == "Save and New")
            {
                this.ResetForm();
            }
        }
        private void ResetForm()
        {
            Helper helper = new Helper();
            helper.Clear(this);

            update = false;
        }
        private void dlgSupplier_Load(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            action = "Save";
            this.Save();
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            action = "Save and New";
            this.Save();
        }

        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            action = "Save and Close";
            this.Save();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
