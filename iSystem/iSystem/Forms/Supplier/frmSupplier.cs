﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class frmSupplier : MetroFramework.Forms.MetroForm
    {
        public frmSupplier()
        {
            InitializeComponent();
        }
        Supplier supplier = new Supplier();
        List<Supplier> suppliers = new List<Supplier>();
        private void Render()
        {
            suppliers.Clear();
            lv.Items.Clear();
            txtSearch.Text = string.Empty;
            txtSearch.Focus();
            supplier.Active = 1;
            suppliers = supplier.Read();
            foreach (var item in suppliers)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Name);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Detail);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Note);
            }
        }
        private void Search()
        {
            suppliers.Clear();
            lv.Items.Clear();
            supplier.Active = 1;
            supplier.Name = txtSearch.Text;
            suppliers = supplier.Search();
            foreach (var item in suppliers)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Name);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Detail);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Note);
            }
        }
        private void frmSupplier_Load(object sender, EventArgs e)
        {
            this.Render();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            dlgSupplier dlgSupplier = new dlgSupplier();
            dlgSupplier.ShowDialog();
            this.Render();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count > 0)
            {
                dlgSupplier dlgSupplier = new dlgSupplier();
                dlgSupplier.Edit(int.Parse(lv.SelectedItems[0].Text));
                dlgSupplier.ShowDialog();
                this.Render();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Confirmation.Delete(lv.SelectedItems[0].SubItems[1].Text))
            {
                supplier.Id = int.Parse(lv.SelectedItems[0].Text);
                supplier.Active = 0;
                supplier.Delete();
                this.Render();
            }
        }
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.Search();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
