﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class dlgMaker : MetroFramework.Forms.MetroForm
    {
        public dlgMaker()
        {
            InitializeComponent();
        }
        Maker maker = new Maker();
        List<Maker> makers = new List<Maker>();
        string action;
        bool update = false;
        public void Edit(int id)
        {
            makers.Clear();
            maker.Id = id;
            makers = maker.GetById();
            txtDesc.Text = makers[0].Description;
            update = true;
        }
        private void Save()
        {
            ValidateLibrary validate = new ValidateLibrary();
            validate.Required(txtDesc);
            if (validate.WithError())
            {
                Messages.Instance.Error("Please correct all required fields.");
                return;
            }
            maker.Description = txtDesc.Text;
            if (update)
            {
                makers.Clear();
                makers = maker.GetById();
                maker.Update();
            }
            else
            {
                maker.Save();
            }
            if (action == "Save")
            {
                update = true;
            }
            else if (action == "Save and Close")
            {
                this.Close();
                this.Dispose();
            }
            else if (action == "Save and New")
            {
                this.ResetForm();
            }
        }
        private void ResetForm()
        {
            Helper helper = new Helper();
            helper.Clear(this);

            update = false;
        }
        private void dlgMaker_Load(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            action = "Save";
            this.Save();
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            action = "Save and New";
            this.Save();
        }

        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            action = "Save and Close";
            this.Save();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
