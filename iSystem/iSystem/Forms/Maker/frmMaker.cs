﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class frmMaker : MetroFramework.Forms.MetroForm
    {
        public frmMaker()
        {
            InitializeComponent();
        }
        Maker maker = new Maker();
        List<Maker> makers = new List<Maker>();
        private void Render()
        {
            makers.Clear();
            lv.Items.Clear();
            txtSearch.Text = string.Empty;
            txtSearch.Focus();
            maker.Active = 1;
            makers = maker.Read();
            foreach (var item in makers)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Description);
            }
        }
        private void Search()
        {
            makers.Clear();
            lv.Items.Clear();
            maker.Active = 1;
            maker.Description = txtSearch.Text;
            makers = maker.Search();
            foreach (var item in makers)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Description);
            }
        }
        private void frmMaker_Load(object sender, EventArgs e)
        {
            this.Render();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            dlgMaker dlgMaker = new dlgMaker();
            dlgMaker.ShowDialog();
            this.Render();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count > 0)
            {
                dlgMaker dlgMaker = new dlgMaker();
                dlgMaker.Edit(int.Parse(lv.SelectedItems[0].Text));
                dlgMaker.ShowDialog();
                this.Render();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Confirmation.Delete(lv.SelectedItems[0].SubItems[1].Text))
            {
                maker.Id = int.Parse(lv.SelectedItems[0].Text);
                maker.Active = 0;
                maker.Delete();
                this.Render();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void btnClose_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.Search();
        }
    }
}
