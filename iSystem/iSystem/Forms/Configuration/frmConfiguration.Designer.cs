﻿namespace iSystem
{
    partial class frmConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDSN = new MetroFramework.Controls.MetroTextBox();
            this.txtHostName = new MetroFramework.Controls.MetroTextBox();
            this.txtDatabsae = new MetroFramework.Controls.MetroTextBox();
            this.txtUsername = new MetroFramework.Controls.MetroTextBox();
            this.txtPassword = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.btnSave = new MetroFramework.Controls.MetroButton();
            this.btnClose = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // txtDSN
            // 
            // 
            // 
            // 
            this.txtDSN.CustomButton.Image = null;
            this.txtDSN.CustomButton.Location = new System.Drawing.Point(166, 1);
            this.txtDSN.CustomButton.Name = "";
            this.txtDSN.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtDSN.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDSN.CustomButton.TabIndex = 1;
            this.txtDSN.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDSN.CustomButton.UseSelectable = true;
            this.txtDSN.CustomButton.Visible = false;
            this.txtDSN.Lines = new string[0];
            this.txtDSN.Location = new System.Drawing.Point(107, 63);
            this.txtDSN.MaxLength = 32767;
            this.txtDSN.Name = "txtDSN";
            this.txtDSN.PasswordChar = '\0';
            this.txtDSN.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDSN.SelectedText = "";
            this.txtDSN.SelectionLength = 0;
            this.txtDSN.SelectionStart = 0;
            this.txtDSN.ShortcutsEnabled = true;
            this.txtDSN.Size = new System.Drawing.Size(188, 23);
            this.txtDSN.TabIndex = 0;
            this.txtDSN.UseSelectable = true;
            this.txtDSN.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtDSN.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtHostName
            // 
            // 
            // 
            // 
            this.txtHostName.CustomButton.Image = null;
            this.txtHostName.CustomButton.Location = new System.Drawing.Point(166, 1);
            this.txtHostName.CustomButton.Name = "";
            this.txtHostName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtHostName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtHostName.CustomButton.TabIndex = 1;
            this.txtHostName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtHostName.CustomButton.UseSelectable = true;
            this.txtHostName.CustomButton.Visible = false;
            this.txtHostName.Lines = new string[0];
            this.txtHostName.Location = new System.Drawing.Point(107, 92);
            this.txtHostName.MaxLength = 32767;
            this.txtHostName.Name = "txtHostName";
            this.txtHostName.PasswordChar = '\0';
            this.txtHostName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtHostName.SelectedText = "";
            this.txtHostName.SelectionLength = 0;
            this.txtHostName.SelectionStart = 0;
            this.txtHostName.ShortcutsEnabled = true;
            this.txtHostName.Size = new System.Drawing.Size(188, 23);
            this.txtHostName.TabIndex = 1;
            this.txtHostName.UseSelectable = true;
            this.txtHostName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtHostName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtDatabsae
            // 
            // 
            // 
            // 
            this.txtDatabsae.CustomButton.Image = null;
            this.txtDatabsae.CustomButton.Location = new System.Drawing.Point(166, 1);
            this.txtDatabsae.CustomButton.Name = "";
            this.txtDatabsae.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtDatabsae.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDatabsae.CustomButton.TabIndex = 1;
            this.txtDatabsae.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDatabsae.CustomButton.UseSelectable = true;
            this.txtDatabsae.CustomButton.Visible = false;
            this.txtDatabsae.Lines = new string[0];
            this.txtDatabsae.Location = new System.Drawing.Point(107, 121);
            this.txtDatabsae.MaxLength = 32767;
            this.txtDatabsae.Name = "txtDatabsae";
            this.txtDatabsae.PasswordChar = '\0';
            this.txtDatabsae.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDatabsae.SelectedText = "";
            this.txtDatabsae.SelectionLength = 0;
            this.txtDatabsae.SelectionStart = 0;
            this.txtDatabsae.ShortcutsEnabled = true;
            this.txtDatabsae.Size = new System.Drawing.Size(188, 23);
            this.txtDatabsae.TabIndex = 2;
            this.txtDatabsae.UseSelectable = true;
            this.txtDatabsae.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtDatabsae.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtUsername
            // 
            // 
            // 
            // 
            this.txtUsername.CustomButton.Image = null;
            this.txtUsername.CustomButton.Location = new System.Drawing.Point(166, 1);
            this.txtUsername.CustomButton.Name = "";
            this.txtUsername.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtUsername.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtUsername.CustomButton.TabIndex = 1;
            this.txtUsername.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtUsername.CustomButton.UseSelectable = true;
            this.txtUsername.CustomButton.Visible = false;
            this.txtUsername.Lines = new string[0];
            this.txtUsername.Location = new System.Drawing.Point(107, 150);
            this.txtUsername.MaxLength = 32767;
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.PasswordChar = '\0';
            this.txtUsername.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUsername.SelectedText = "";
            this.txtUsername.SelectionLength = 0;
            this.txtUsername.SelectionStart = 0;
            this.txtUsername.ShortcutsEnabled = true;
            this.txtUsername.Size = new System.Drawing.Size(188, 23);
            this.txtUsername.TabIndex = 3;
            this.txtUsername.UseSelectable = true;
            this.txtUsername.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtUsername.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtPassword
            // 
            // 
            // 
            // 
            this.txtPassword.CustomButton.Image = null;
            this.txtPassword.CustomButton.Location = new System.Drawing.Point(166, 1);
            this.txtPassword.CustomButton.Name = "";
            this.txtPassword.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPassword.CustomButton.TabIndex = 1;
            this.txtPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPassword.CustomButton.UseSelectable = true;
            this.txtPassword.CustomButton.Visible = false;
            this.txtPassword.Lines = new string[0];
            this.txtPassword.Location = new System.Drawing.Point(107, 179);
            this.txtPassword.MaxLength = 32767;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '•';
            this.txtPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPassword.SelectedText = "";
            this.txtPassword.SelectionLength = 0;
            this.txtPassword.SelectionStart = 0;
            this.txtPassword.ShortcutsEnabled = true;
            this.txtPassword.Size = new System.Drawing.Size(188, 23);
            this.txtPassword.TabIndex = 4;
            this.txtPassword.UseSelectable = true;
            this.txtPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(63, 63);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(38, 19);
            this.metroLabel1.TabIndex = 5;
            this.metroLabel1.Text = "DSN:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(23, 92);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(78, 19);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Host Name:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(35, 121);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(66, 19);
            this.metroLabel3.TabIndex = 7;
            this.metroLabel3.Text = "Database:";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(30, 150);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(71, 19);
            this.metroLabel4.TabIndex = 8;
            this.metroLabel4.Text = "Username:";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(35, 179);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(66, 19);
            this.metroLabel5.TabIndex = 9;
            this.metroLabel5.Text = "Password:";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(139, 208);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 42);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseSelectable = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(220, 208);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 42);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Close";
            this.btnClose.UseSelectable = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frmConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 272);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.txtDatabsae);
            this.Controls.Add(this.txtHostName);
            this.Controls.Add(this.txtDSN);
            this.Name = "frmConfiguration";
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox txtDSN;
        private MetroFramework.Controls.MetroTextBox txtHostName;
        private MetroFramework.Controls.MetroTextBox txtDatabsae;
        private MetroFramework.Controls.MetroTextBox txtUsername;
        private MetroFramework.Controls.MetroTextBox txtPassword;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroButton btnSave;
        private MetroFramework.Controls.MetroButton btnClose;
    }
}

