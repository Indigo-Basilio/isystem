﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class frmConfiguration : MetroFramework.Forms.MetroForm
    {
        public frmConfiguration()
        {
            InitializeComponent();
        }

        private void GetServerSetting()
        {
            txtDSN.Text = iSystem.Configuration.DSN;
            txtHostName.Text = iSystem.Configuration.DB_HOST;
            txtDatabsae.Text = iSystem.Configuration.DB_NAME;
            txtUsername.Text = iSystem.Configuration.DB_USER;
            txtPassword.Text = iSystem.Configuration.DB_PASSWORD;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!(iSystem.Configuration.DSN == string.Empty))
            {
                this.GetServerSetting();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(this.CheckRequiredFields() == true)
            {
                //Set setting
                iSystem.Configuration.DSN = txtDSN.Text;
                iSystem.Configuration.DB_HOST = txtHostName.Text;
                iSystem.Configuration.DB_NAME = txtDatabsae.Text;
                iSystem.Configuration.DB_USER = txtUsername.Text;
                iSystem.Configuration.DB_PASSWORD = txtPassword.Text;

                //Save setting
                iSystem.Configuration.saveSettings();

                if (iSystem.Configuration.TestConnection())
                {
                    MessageBox.Show("Server database configuration has been successfully updated", "System Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    this.Dispose();
                }

            }
            else
            {
                MessageBox.Show("Please fill out all required fields", "System Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private bool CheckRequiredFields()
        {
            if (txtDSN.Text == "")
            {
                return false;
            }
            if (txtHostName.Text == "")
            {
                return false;
            }
            if (txtDatabsae.Text == "")
            {
                return false;
            }
            if (txtUsername.Text == "")
            {
                return false;
            }
            
            return true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
