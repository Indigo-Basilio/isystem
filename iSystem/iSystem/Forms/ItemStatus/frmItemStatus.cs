﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class frmItemStatus : MetroFramework.Forms.MetroForm
    {
        public frmItemStatus()
        {
            InitializeComponent();
        }
        ItemStatus _item = new ItemStatus();
        List<ItemStatus> _items = new List<ItemStatus>();
        private void Render()
        {
            _items.Clear();
            lv.Items.Clear();
            txtSearch.Text = string.Empty;
            txtSearch.Focus();
            _item.Active = 1;
            _items = _item.Read();
            foreach (var item in _items)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Description);
            }
        }
        private void Search()
        {
            _items.Clear();
            lv.Items.Clear();
            _item.Active = 1;
            _item.Description = txtSearch.Text;
            _items = _item.Search();
            foreach (var item in _items)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Description);
            }
        }
        private void frmItemStatus_Load(object sender, EventArgs e)
        {
            this.Render();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            dlgItemStatus dlgItemStatus = new dlgItemStatus();
            dlgItemStatus.ShowDialog();
            this.Render();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count > 0)
            {
                dlgItemStatus dlgItemStatus = new dlgItemStatus();
                dlgItemStatus.Edit(int.Parse(lv.SelectedItems[0].Text));
                dlgItemStatus.ShowDialog();
                this.Render();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Confirmation.Delete(lv.SelectedItems[0].SubItems[1].Text))
            {
                _item.Id = int.Parse(lv.SelectedItems[0].Text);
                _item.Active = 0;
                _item.Delete();
                this.Render();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.Search();
        }
    }
}
