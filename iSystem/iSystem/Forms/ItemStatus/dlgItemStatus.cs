﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class dlgItemStatus : MetroFramework.Forms.MetroForm
    {
        public dlgItemStatus()
        {
            InitializeComponent();
        }
        ItemStatus _item = new ItemStatus();
        List<ItemStatus> _items = new List<ItemStatus>();
        string action;
        bool update = false;
        private void dlgItemStatus_Load(object sender, EventArgs e)
        {

        }
        public void Edit(int id)
        {
            _items.Clear();
            _item.Id = id;
            _items = _item.GetById();
            txtDesc.Text = _items[0].Description;
            update = true;
        }
        private void Save()
        {
            ValidateLibrary validate = new ValidateLibrary();
            validate.Required(txtDesc);
            if (validate.WithError())
            {
                Messages.Instance.Error("Please correct all required fields.");
                return;
            }
            _item.Description = txtDesc.Text;
            if (update)
            {
                _items.Clear();
                _items = _item.GetById();
                _item.Update();
            }
            else
            {
                _item.Save();
            }
            if (action == "Save")
            {
                update = true;
            }
            else if (action == "Save and Close")
            {
                this.Close();
                this.Dispose();
            }
            else if (action == "Save and New")
            {
                this.ResetForm();
            }
        }
        private void ResetForm()
        {
            Helper helper = new Helper();
            helper.Clear(this);

            update = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            action = "Save";
            this.Save();
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            action = "Save and New";
            this.Save();
        }

        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            action = "Save and Close";
            this.Save();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
