﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class dlgBundle : MetroFramework.Forms.MetroForm
    {
        public dlgBundle()
        {
            InitializeComponent();
        }
        Location location = new Location();
        Bundle bundle = new Bundle();
        List<Location> locations = new List<Location>();
        List<Bundle> bundles = new List<Bundle>();
        string action;
        bool update = false;
        private void renderLocation()
        {
            locations.Clear();
            cbxLocation.Items.Clear();
            location.Active = 1;
            locations = location.Read();
            foreach (var item in locations)
            {
                cbxLocation.Items.Add(item.Description);
            }
        }
        private void dlgBundle_Load(object sender, EventArgs e)
        {
            this.renderLocation();
        }
        public void Edit(int id)
        {
            bundles.Clear();
            cbSystemUnit.Checked = false;
            cbAllowBorrow.Checked = false;
            bundle.Id = id;                   
            bundles = bundle.GetById();            
            txtCode.Text = bundles[0].Code;
            txtDescription.Text = bundles[0].Description;
            if(bundles[0].SystemUnit == 1)
            {
                cbSystemUnit.Checked = true;
            }
            if(bundles[0].AllowBorrow == 1)
            {
                cbAllowBorrow.Checked = true;
            }
            cbxLocation.Text = bundles[0].LocationDescription;

            update = true;

        }
        private void Save()
        {
            ValidateLibrary validate = new ValidateLibrary();
            validate.Required(txtCode);
            validate.Required(txtDescription);
            validate.Required(cbxLocation);

            if (validate.WithError())
            {
                Messages.Instance.Error("Please correct all required fields.");
                return;
            }            
            bundle.Code = txtCode.Text;
            bundle.Description = txtDescription.Text;

            if (cbSystemUnit.Checked)
            {
                bundle.SystemUnit = 1;
            }
            else
            {
                bundle.SystemUnit = 0;
            }
            if (cbAllowBorrow.Checked)
            {
                bundle.AllowBorrow = 1;
            }
            else
            {
                bundle.AllowBorrow = 0;
            }

            location.Description = cbxLocation.Text;
            bundle.LocationId = location.GetByName();
            if (update)
            {
                bundles.Clear();
                bundles = bundle.GetById();
                bundle.Update();
            }
            else
            {
                bundle.Save();
            }
            if (action == "Save")
            {
                update = true;
            }
            else if (action == "Save and Close")
            {
                this.Close();
                this.Dispose();
            }
            else if (action == "Save and New")
            {
                this.ResetForm();
            }
        }
        private void ResetForm()
        {
            Helper helper = new Helper();
            helper.Clear(this);

            update = false;
        }
        private void btnLocation_Click(object sender, EventArgs e)
        {
            frmLocation frmLocation = new frmLocation();
            frmLocation.ShowDialog();
            this.renderLocation();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            action = "Save";
            this.Save();
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            action = "Save and New";
            this.Save();
        }

        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            action = "Save and Close";
            this.Save();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
