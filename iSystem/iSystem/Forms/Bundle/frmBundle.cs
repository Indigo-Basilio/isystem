﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class frmBundle : MetroFramework.Forms.MetroForm
    {
        public frmBundle()
        {
            InitializeComponent();
        }
        Bundle bundle = new Bundle();
        List<Bundle> bundles = new List<Bundle>();
        private void Render()
        {
            bundles.Clear();
            lv.Items.Clear();
            txtSearch.Text = string.Empty;
            txtSearch.Focus();
            bundle.Active = 1;
            bundles = bundle.Read();
            foreach (var item in bundles)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Code);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Description);
                if(item.SystemUnit == 1)
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("YES");
                }
                else
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("NO");
                }
                if (item.AllowBorrow == 1)
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("YES");
                }
                else
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("NO");
                }
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.LocationDescription);
            }
        }
        private void Search()
        {
            bundles.Clear();
            lv.Items.Clear();
            bundle.Active = 1;
            bundle.Code = txtSearch.Text;
            bundles = bundle.Search();
            foreach (var item in bundles)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Code);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Description);
                if (item.SystemUnit == 1)
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("YES");
                }
                else
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("NO");
                }
                if (item.AllowBorrow == 1)
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("YES");
                }
                else
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("NO");
                }
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.LocationDescription);
            }
        }
        private void frmBundle_Load(object sender, EventArgs e)
        {
            this.Render();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            dlgBundle dlgBundle = new dlgBundle();
            dlgBundle.ShowDialog();
            this.Render();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count > 0)
            {
                dlgBundle dlgBundle = new dlgBundle();
                dlgBundle.Edit(int.Parse(lv.SelectedItems[0].Text));
                dlgBundle.ShowDialog();
                this.Render();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count > 0)
            {
                if (Confirmation.Delete(lv.SelectedItems[0].SubItems[1].Text))
                {
                    bundle.Id = int.Parse(lv.SelectedItems[0].Text);
                    bundle.Active = 0;
                    bundle.Delete();
                    this.Render();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.Search();
        }
    }
}
