﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    public partial class frmItem : MetroFramework.Forms.MetroForm
    {
        public frmItem()
        {
            InitializeComponent();
        }
        ItemType ItemType = new ItemType();
        Maker Maker = new Maker();
        Bundle Bundle = new Bundle();
        Supplier Supplier = new Supplier();
        Location Location = new Location();
        ItemStatus ItemStatus = new ItemStatus();
        ItemSTI ItemSTI = new ItemSTI();
        List<ItemType> ItemTypes = new List<ItemType>();
        List<Maker> Makers = new List<Maker>();
        List<Bundle> Bundles = new List<Bundle>();
        List<Supplier> Suppliers = new List<Supplier>();
        List<Location> Locations = new List<Location>();
        List<ItemStatus> ItemStatuss = new List<ItemStatus>();
        List<ItemSTI> ItemSTIs = new List<ItemSTI>();
        private void Render()
        {
            ItemSTIs.Clear();
            lv.Items.Clear();
            txtSearch.Text = string.Empty;
            txtSearch.Focus();
            ItemSTI.Active = 1;
            ItemSTIs = ItemSTI.Read();
            foreach (var item in ItemSTIs)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Code);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Description);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.SubDescription);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Serial);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.DateReceived);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.DatePurchased);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.SupplierName);
                if(item.Inbundle == 1)
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("YES");
                }
                else
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("NO");
                    lv.Items[lv.Items.Count - 1].SubItems.Add("NONE");
                }

                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Price.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.OrNumber);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.ItemTypeDescription);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.MakerDescription);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.LocationDescription);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.StatusDescription);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Remarks);
                if(item.AllowBorrow == 1)
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("YES");
                }
                else
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("NO");
                }
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.DateLastInventory);
            }
        }
        private void Search()
        {
            ItemSTIs.Clear();
            lv.Items.Clear();
            ItemSTI.Active = 1;
            ItemSTI.Code = txtSearch.Text;
            ItemSTIs = ItemSTI.Search();
            foreach (var item in ItemSTIs)
            {
                lv.Items.Add(item.Id.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Code);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Description);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.SubDescription);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Serial);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.DateReceived);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.DatePurchased);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.SupplierName);
                if (item.Inbundle == 1)
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("YES");
                }
                else
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("NO");
                    lv.Items[lv.Items.Count - 1].SubItems.Add("NONE");
                }

                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Price.ToString());
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.OrNumber);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.ItemTypeDescription);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.MakerDescription);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.LocationDescription);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.StatusDescription);
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.Remarks);
                if (item.AllowBorrow == 1)
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("YES");
                }
                else
                {
                    lv.Items[lv.Items.Count - 1].SubItems.Add("NO");
                }
                lv.Items[lv.Items.Count - 1].SubItems.Add(item.DateLastInventory);
            }
        }
        private void frmItem_Load(object sender, EventArgs e)
        {
            this.Render();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.Search();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            dlgItem dlgItem = new dlgItem();
            dlgItem.ShowDialog();
            this.Render();
        }
    }
}
