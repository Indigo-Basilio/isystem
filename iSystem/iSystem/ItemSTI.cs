﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSystem
{
    class ItemSTI
    {
        /****************************
        * Protected Properties
        * **************************/
        protected int _id;
        protected string _code;
        protected string _description;
        protected string _subdescription;
        protected string _serial;
        protected string _dateReceived;
        protected string _datePurchased;
        protected string _warrantyTo;
        protected int _supplierId;
        protected string _supplierName;
        protected int _inBundle;
        protected int _bundleId;
        protected string _bundleDescription;
        protected double _price;
        protected string _orNumber;
        protected int _itemTypeId;
        protected string _itemTypeDescription;
        protected int _makerId;
        protected string _makerDescription;
        protected int _locationId;
        protected string _locationDescription;
        protected int _itemStatusId;
        protected string _itemStatusDescription;
        protected string _remarks;
        protected int _allowBorrow;
        protected byte[] _itemImage;
        protected string _enteredBy;
        protected string _dateEntered;
        protected string _inventoryBy;
        protected string _dateLastInventory;
        protected int _active;      
        protected List<ItemSTI> _items = new List<ItemSTI>();
        /****************************
        * Public Properties
        * **************************/
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string SubDescription
        {
            get { return _subdescription; }
            set { _subdescription = value; }
        }
        public string Serial
        {
            get { return _serial; }
            set { _serial = value; }
        }
        public string DateReceived
        {
            get { return _dateReceived; }
            set { _dateReceived = value; }
        }
        public string DatePurchased
        {
            get { return _datePurchased; }
            set { _datePurchased = value; }
        }
        public string WarrantyTo
        {
            get { return _warrantyTo; }
            set { _warrantyTo = value; }
        }
        public int SupplierId
        {
            get { return _supplierId; }
            set { _supplierId = value; }
        }
        public int Inbundle
        {
            get { return _inBundle; }
            set { _inBundle = value; }
        }
        public int BundleId
        {
            get { return _bundleId; }
            set { _bundleId = value; }
        }
        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public string OrNumber
        {
            get { return _orNumber; }
            set { _orNumber = value; }
        }
        public int ItemTypeId
        {
            get { return _itemTypeId; }
            set { _itemTypeId = value; }
        }
        public int MakerId
        {
            get { return _makerId; }
            set { _makerId = value; }
        }
        public int LocationId
        {
            get { return _locationId; }
            set { _locationId = value; }
        }
        public int StatusId
        {
            get { return _itemStatusId; }
            set { _itemStatusId = value; }
        }
        public string Remarks
        {
            get { return _remarks; }
            set { _remarks = value; }
        }
        public int AllowBorrow
        {
            get { return _allowBorrow; }
            set { _allowBorrow = value; }
        }
        public byte[] Image
        {
            get { return _itemImage; }
            set { _itemImage = value; }
        }
        public string EnteredBy
        {
            get { return _enteredBy; }
            set { _enteredBy = value; }
        }
        public string DateEntered
        {
            get { return _dateEntered; }
            set { _dateEntered = value; }
        }
        public string InventoryBy
        {
            get { return _inventoryBy; }
            set { _inventoryBy = value; }
        }
        public string DateLastInventory
        {
            get { return _dateLastInventory; }
            set { _dateLastInventory = value; }
        }
        public int Active
        {
            get { return _active; }
            set { _active = value; }
        }
        public string SupplierName
        {
            get { return _supplierName; }
            set { _supplierName = value; }
        }
        public string BundleDescription
        {
            get { return _bundleDescription; }
            set { _bundleDescription = value; }
        }
        public string ItemTypeDescription
        {
            get { return _itemTypeDescription; }
            set { _itemTypeDescription = value; }
        }
        public string MakerDescription
        {
            get { return _makerDescription; }
            set { _makerDescription = value; }
        }
        public string LocationDescription
        {
            get { return _locationDescription; }
            set { _locationDescription = value; }
        }
        public string StatusDescription
        {
            get { return _itemStatusDescription; }
            set { _itemStatusDescription = value; }
        }       
        /****************************
        * Public Method
        * **************************/
        public void Save()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "INSERT INTO item(item_code,item_description,item_sub_description,item_serial,date_received,date_purchased,warranty_to,supplier_id,inbundle,bundle_id,item_price,or_no,item_type_id,maker_id,location_id,status_id,remarks,allow_borrow,item_image,entered_by,inventory_by,date_last_inventory) VALUES(@item_code,@item_description,@item_sub_description,@item_serial,@date_received,@date_purchased,@warranty_to,@supplier_id,@inbundle,@bundle_id,@item_price,@or_no,@item_type_id,@maker_id,@location_id,@status_id,@remarks,@allow_borrow,@item_image,@entered_by,@inventory_by,@date_last_inventory)";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("item_code", _code);
                    cmd.Parameters.AddWithValue("item_description", _description);
                    cmd.Parameters.AddWithValue("item_sub_description", _subdescription);
                    cmd.Parameters.AddWithValue("item_serial", _serial );
                    cmd.Parameters.AddWithValue("date_received", _dateReceived);
                    cmd.Parameters.AddWithValue("date_purchased", _datePurchased);
                    cmd.Parameters.AddWithValue("warranty_to", _warrantyTo);
                    cmd.Parameters.AddWithValue("supplier_id", _supplierId);
                    cmd.Parameters.AddWithValue("inbundle", _inBundle);
                    cmd.Parameters.AddWithValue("item_price", _price);
                    cmd.Parameters.AddWithValue("or_no", _orNumber);
                    cmd.Parameters.AddWithValue("item_type_id", _itemTypeId);
                    cmd.Parameters.AddWithValue("bundle_id", _bundleId);
                    cmd.Parameters.AddWithValue("maker_id", _makerId);
                    cmd.Parameters.AddWithValue("location_id", _locationId);
                    cmd.Parameters.AddWithValue("status_id", _itemStatusId);
                    cmd.Parameters.AddWithValue("remarks", _remarks);
                    cmd.Parameters.AddWithValue("allow_borrow", _allowBorrow);
                    cmd.Parameters.AddWithValue("item_image", _itemImage);
                    cmd.Parameters.AddWithValue("entered_by", _enteredBy);
                    cmd.Parameters.AddWithValue("inventory_by", _inventoryBy);
                    cmd.Parameters.AddWithValue("date_last_inventory", _dateLastInventory);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Saved!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public List<ItemSTI> Read()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT item.*,supplier.supp_name as supplier,bundle.bundle_description as bundle,item_type.type_description as type ,maker.maker_description as maker,location.location_description as location,item_status.status_description as istatus FROM item INNER JOIN supplier INNER JOIN bundle INNER JOIN item_type INNER JOIN maker INNER JOIN location INNER JOIN item_status ON (item.supplier_id = supplier.id AND item.bundle_id = bundle.id AND item.item_type_id = item_type.id AND item.maker_id = maker.id AND item.location_id = location.id AND item.status_id = item_status.id) WHERE item.active = @active ORDER BY item.item_code ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        ItemSTI item = new ItemSTI();
                        item._id = int.Parse(dr["id"].ToString());
                        item._code = dr["item_code"].ToString();
                        item._description = dr["item_description"].ToString();
                        item._subdescription = dr["item_sub_description"].ToString();
                        item._serial = dr["item_serial"].ToString();
                        item._dateReceived = dr["date_received"].ToString();
                        item._datePurchased = dr["date_purchased"].ToString();
                        item._warrantyTo = dr["warranty_to"].ToString();
                        item._supplierName = dr["supplier"].ToString();//supplier
                        item._inBundle = int.Parse(dr["inbundle"].ToString());
                        item._bundleDescription = dr["bundle"].ToString();
                        item._price = double.Parse(dr["item_price"].ToString());
                        item._orNumber = dr["or_no"].ToString();
                        item._itemTypeDescription = dr["type"].ToString();
                        item._makerDescription = dr["maker"].ToString();         
                        item._locationDescription = dr["location"].ToString();
                        item._itemStatusDescription = dr["istatus"].ToString();
                        item._remarks = dr["remarks"].ToString();
                        item._allowBorrow = int.Parse(dr["allow_borrow"].ToString());
                        //item._itemImage = dr["item_image"].ToString();
                        item._enteredBy = dr["entered_by"].ToString();
                        item._dateEntered = dr["date_entered"].ToString();
                        item._inventoryBy = dr["inventory_by"].ToString();
                        item._dateLastInventory = dr["date_last_inventory"].ToString();
                        item._active = int.Parse(dr["active"].ToString());                        
                        _items.Add(item);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _items;
        }
        public List<ItemSTI> Search()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT item.*,supplier.supp_name as supplier,bundle.bundle_description as bundle,item_type.type_description as type ,maker.maker_description as maker,location.location_description as location,item_status.status_description as istatus FROM item INNER JOIN supplier INNER JOIN bundle INNER JOIN item_type INNER JOIN maker INNER JOIN location INNER JOIN item_status ON (item.supplier_id = supplier.id AND item.bundle_id = bundle.id AND item.item_type_id = item_type.id AND item.maker_id = maker.id AND item.location_id = location.id AND item.status_id = item_status.id) WHERE item.active = @active AND item.item_code LIKE @code OR item.item_description = @code ORDER BY item.item_code ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    cmd.Parameters.AddWithValue("code", "%" + _code + "%");
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        ItemSTI item = new ItemSTI();
                        item._id = int.Parse(dr["id"].ToString());
                        item._code = dr["item_code"].ToString();
                        item._description = dr["item_description"].ToString();
                        item._subdescription = dr["item_sub_description"].ToString();
                        item._serial = dr["item_serial"].ToString();
                        item._dateReceived = dr["date_received"].ToString();
                        item._datePurchased = dr["date_purchased"].ToString();
                        item._warrantyTo = dr["warranty_to"].ToString();
                        item._supplierName = dr["supplier"].ToString();//supplier
                        item._inBundle = int.Parse(dr["inbundle"].ToString());
                        item._bundleDescription = dr["bundle"].ToString();
                        item._price = double.Parse(dr["item_price"].ToString());
                        item._orNumber = dr["or_no"].ToString();
                        item._itemTypeDescription = dr["type"].ToString();
                        item._makerDescription = dr["maker"].ToString();
                        item._locationDescription = dr["location"].ToString();
                        item._itemStatusDescription = dr["istatus"].ToString();
                        item._remarks = dr["remarks"].ToString();
                        item._allowBorrow = int.Parse(dr["allow_borrow"].ToString());
                        //item._itemImage = dr["item_image"].ToString();
                        item._enteredBy = dr["entered_by"].ToString();
                        item._dateEntered = dr["date_entered"].ToString();
                        item._inventoryBy = dr["inventory_by"].ToString();
                        item._dateLastInventory = dr["date_last_inventory"].ToString();
                        item._active = int.Parse(dr["active"].ToString());
                        _items.Add(item);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _items;
        }
        public List<ItemSTI> GetById()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT item.*,supplier.name,bundle.bundle_description,item_type.type_description,maker.maker_description,location.location_description,item_status.status_description FROM item INNER JOIN supplier INNER JOIN bundle INNER JOIN item_type INNER JOIN maker INNER JOIN location INNER JOIN item_status WHERE item.id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        //,,,,,,,,,,maker_id,location_id,status_id,remarks,allow_borrow,item_image,entered_by,date_entered,inventory_by,date_last_inventory
                        ItemSTI item = new ItemSTI();
                        item._id = int.Parse(dr["id"].ToString());
                        item._code = dr["item_code"].ToString();
                        item._description = dr["item_description"].ToString();
                        item._subdescription = dr["item_sub_description"].ToString();
                        item._serial = dr["item_serial"].ToString();
                        item._dateReceived = dr["date_received"].ToString();
                        item._datePurchased = dr["date_purchased"].ToString();
                        item._warrantyTo = dr["warranty_to"].ToString();
                        item._supplierName = dr["name"].ToString();//supplier
                        item._inBundle = int.Parse(dr["inbundle"].ToString());
                        item._bundleDescription = dr["bundle_description"].ToString();
                        item._price = double.Parse(dr["item_price"].ToString());
                        item._orNumber = dr["or_no"].ToString();
                        item._itemTypeDescription = dr["type_description"].ToString();
                        item._makerDescription = dr["maker_description"].ToString();
                        item._locationDescription = dr["location_description"].ToString();
                        item._itemStatusDescription = dr["status_description"].ToString();
                        item._remarks = dr["remark"].ToString();
                        item._allowBorrow = int.Parse(dr["allow_borrow"].ToString());
                        //item._itemImage = dr["item_image"].ToString();
                        item._enteredBy = dr["entered_by"].ToString();
                        item._dateEntered = dr["date_entered"].ToString();
                        item._inventoryBy = dr["inventory_by"].ToString();
                        item._dateLastInventory = dr["date_last_inventory"].ToString();
                        item._active = int.Parse(dr["active"].ToString());
                        _items.Add(item);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _items;
        }
        public void GetEmployeeImage(ref PictureBox picture)
        {
            try
            {
                //set database connection
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //open connection
                    con.Open();

                    //set query
                    string sql = "SELECT id, item_image FROM item WHERE id = @id";

                    //send query
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("id", _id);

                    //read rows
                    MySqlDataReader reader = cmd.ExecuteReader();

                    //get record
                    while (reader.Read())
                    {
                        try
                        {
                            byte[] img = (byte[])reader["item_image"];
                            picture.Image = ImageLibrary.Instance.byteArrayToImage(img);
                        }
                        catch (Exception)
                        {

                        }

                    }
                }
            }
            catch (MySqlException ex)
            {
                //error loading of picture
                MessageBox.Show(ex.Message.ToString());
            }
        }
        public void Update()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE item SET item_code = @item_code,item_description = @item_description,item_sub_description = @item_sub_description, = @item_serial,date_received = @date_received,date_purchased = @date_purchased,warranty_to = @warranty_to,supplier_id = @supplier_id,inbundle = @inbundle,bundle_id = @bundle_id,item_price = @item_price,or_no = @or_no,item_type_id = @item_type_id,maker_id = @maker_id,location_id = @location_id,status_id = @status_id,remarks = @remarks,allow_borrow = @allow_borrow,item_image = @item_image,inventory_by = @inventory_by,date_last_inventory = @date_last_inventory WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("item_code", _code);
                    cmd.Parameters.AddWithValue("item_description", _description);
                    cmd.Parameters.AddWithValue("item_sub_description", _subdescription);
                    cmd.Parameters.AddWithValue("item_serial", _serial);
                    cmd.Parameters.AddWithValue("date_received", _dateReceived);
                    cmd.Parameters.AddWithValue("date_purchased", _datePurchased);
                    cmd.Parameters.AddWithValue("warranty_to", _warrantyTo);
                    cmd.Parameters.AddWithValue("supplier_id", _supplierId);
                    cmd.Parameters.AddWithValue("inbundle", _inBundle);
                    cmd.Parameters.AddWithValue("item_price", _price);
                    cmd.Parameters.AddWithValue("or_no", _orNumber);
                    cmd.Parameters.AddWithValue("item_type_id", _itemTypeId);
                    cmd.Parameters.AddWithValue("bundle_id", _bundleId);
                    cmd.Parameters.AddWithValue("maker_id", _makerId);
                    cmd.Parameters.AddWithValue("location_id", _locationId);
                    cmd.Parameters.AddWithValue("status_id", _itemStatusId);
                    cmd.Parameters.AddWithValue("remarks", _remarks);
                    cmd.Parameters.AddWithValue("allow_borrow", _allowBorrow);
                    cmd.Parameters.AddWithValue("item_image", _itemImage);
                    cmd.Parameters.AddWithValue("inventory_by", _inventoryBy);
                    cmd.Parameters.AddWithValue("date_last_inventory", _dateLastInventory);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Updated!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public void Delete()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE item SET active = @active WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("active", _active);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Deleted!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public void GetDateAndTimeDatabase()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open connection
                    con.Open();

                    //Prepare SQL
                    string sql = "SELECT NOW() as indigo";

                    //Set command
                    MySqlCommand cmd = new MySqlCommand(sql, con);

                    //Read
                    MySqlDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        _dateLastInventory = dr["indigo"].ToString();
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
      
        
    }
}
