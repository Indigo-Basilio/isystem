﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iSystem
{
    class Bundle
    {
        /****************************
       * Protected Properties
       * **************************/
        protected int _id;
        protected string _code;
        protected string _description;
        protected int _systemUnit;
        protected int _allowBorrow;
        protected int _locationId;
        protected string _locationDescription;
        protected int _active;
        protected List<Bundle> bundles = new List<Bundle>();
        /****************************
        * Public Properties
        * **************************/
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public int SystemUnit
        {
            get { return _systemUnit; }
            set { _systemUnit = value; }
        }
        public int AllowBorrow
        {
            get { return _allowBorrow; }
            set { _allowBorrow = value; }
        }
        public int LocationId
        {
            get { return _locationId; }
            set { _locationId = value; }
        }
        public string LocationDescription
        {
            get { return _locationDescription; }
            set { _locationDescription = value; }
        }
        public int Active
        {
            get { return _active; }
            set { _active = value; }
        }
        /****************************
   * Public Method
   * **************************/
        public void Save()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "INSERT INTO bundle(bundle_code,bundle_description,system_unit,allow_borrow,location_id) VALUES(@code,@desc,@systemunit,@allowborrow,@locationid)";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("code", _code);
                    cmd.Parameters.AddWithValue("desc", _description);
                    cmd.Parameters.AddWithValue("systemunit", _systemUnit);
                    cmd.Parameters.AddWithValue("allowborrow", _allowBorrow);
                    cmd.Parameters.AddWithValue("locationid", _locationId);
                    
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Saved!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public List<Bundle> Read()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT location.* ,bundle.* FROM bundle INNER JOIN location ON location.id = bundle.location_id WHERE bundle.active = @active ORDER BY bundle_code ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        Bundle bundle = new Bundle();
                        bundle._id = int.Parse(dr["id"].ToString());
                        bundle._code = dr["bundle_code"].ToString();
                        bundle._description = dr["bundle_description"].ToString();
                        bundle._systemUnit = int.Parse(dr["system_unit"].ToString());
                        bundle._allowBorrow = int.Parse(dr["allow_borrow"].ToString());
                        bundle._locationDescription = dr["location_description"].ToString();
                        bundles.Add(bundle);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return bundles;
        }
        public List<Bundle> Search()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT location.* ,bundle.* FROM bundle INNER JOIN location ON location.id = bundle.location_id WHERE bundle.bundle_code LIKE @code OR bundle.bundle_description LIKE @code ORDER BY bundle_code ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    cmd.Parameters.AddWithValue("code", "%" + _code + "%");
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        Bundle bundle = new Bundle();
                        bundle._id = int.Parse(dr["id"].ToString());
                        bundle._code = dr["bundle_code"].ToString();
                        bundle._description = dr["bundle_description"].ToString();
                        bundle._systemUnit = int.Parse(dr["system_unit"].ToString());
                        bundle._allowBorrow = int.Parse(dr["allow_borrow"].ToString());
                        bundle._locationDescription = dr["location_description"].ToString();
                        bundles.Add(bundle);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return bundles;
        }
        public List<Bundle> GetById()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT location.* ,bundle.* FROM bundle INNER JOIN location ON location.id = bundle.location_id WHERE bundle.id = @id ORDER BY bundle_code ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        Bundle bundle = new Bundle();
                        bundle._id = int.Parse(dr["id"].ToString());
                        bundle._code = dr["bundle_code"].ToString();
                        bundle._description = dr["bundle_description"].ToString();
                        bundle._systemUnit = int.Parse(dr["system_unit"].ToString());
                        bundle._allowBorrow = int.Parse(dr["allow_borrow"].ToString());
                        bundle._locationDescription = dr["location_description"].ToString();
                        bundles.Add(bundle);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return bundles;
        }
        public int GetByName()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT location.* ,bundle.* FROM bundle INNER JOIN location ON location.id = bundle.location_id WHERE bundle.bundle_code = @code ORDER BY bundle_code ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("code", _code);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        _id = int.Parse(dr["id"].ToString());
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _id;
        }
        public int GetByNameAgain()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT location.id as location ,bundle.* FROM bundle INNER JOIN location ON location.id = bundle.location_id WHERE bundle.bundle_code = @code ORDER BY bundle_code ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("code", _code);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        _id = int.Parse(dr["location"].ToString());
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _id;
        }
        public void Update()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE bundle SET bundle_code = @code, bundle_description = @desc, system_unit = @systemunit, allow_borrow = @allowborrow, location_id = @locationId WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("code", _code);
                    cmd.Parameters.AddWithValue("desc", _description);
                    cmd.Parameters.AddWithValue("systemunit", _systemUnit);
                    cmd.Parameters.AddWithValue("allowborrow", _allowBorrow);
                    cmd.Parameters.AddWithValue("locationId", _locationId);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Updated!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public void Delete()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE bundle SET active = @active WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("active", _active);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Deleted!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
    }
}
