﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iSystem
{
    class Maker
    {
        /****************************
        * Protected Properties
        * **************************/
        protected int _id;
        protected string _description;
        protected int _active;
        protected List<Maker> makers = new List<Maker>();
        /****************************
        * Public Properties
        * **************************/
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public int Active
        {
            get { return _active; }
            set { _active = value; }
        }
        /****************************
        * Public Method
        * **************************/
        public void Save()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "INSERT INTO maker(maker_description) VALUES(@desc)";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("desc", _description);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Saved!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public List<Maker> Read()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM maker WHERE active = @active ORDER BY maker_description ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        Maker maker= new Maker();
                        maker._id = int.Parse(dr["id"].ToString());
                        maker._description = dr["maker_description"].ToString();
                        makers.Add(maker);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return makers;
        }
        public List<Maker> Search()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM maker WHERE active = @active AND maker_description LIKE @desc ORDER BY maker_description ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    cmd.Parameters.AddWithValue("desc", "%" + _description + "%");
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        Maker maker = new Maker();
                        maker._id = int.Parse(dr["id"].ToString());
                        maker._description = dr["maker_description"].ToString();
                        makers.Add(maker);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return makers;
        }
        public List<Maker> GetById()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM maker WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        Maker maker = new Maker();
                        maker._id = int.Parse(dr["id"].ToString());
                        maker._description = dr["maker_description"].ToString();
                        makers.Add(maker);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return makers;
        }
        public int GetByName()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM maker WHERE maker_description = @desc";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("desc", _description);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        _id = int.Parse(dr["id"].ToString());
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _id;
        }
        public void Update()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE maker SET maker_description = @desc WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("desc", _description); ;
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Updated!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public void Delete()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE maker SET active = @active WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("active", _active);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Deleted!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
    }
}
