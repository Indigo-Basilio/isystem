﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using dtrSTI.Libraries;

namespace iSystem
{
    class Login
    {
        /**********************************************************
         *  Protected Properties
         * ********************************************************/

        protected static int id;
        protected string username;
        protected string password;        
        protected static string firstName = null;
        protected static string lastName = null;
        protected static string middleName = null;
        protected static string node = null;
        protected static string contactNumber = "";
        protected static string email = "";
        protected static string position = "";
        protected bool success = false;

        //list froup for Banks
        protected List<Login> users = new List<Login>();

        /**********************************************************
         *  Private Properties
         * ********************************************************/

        /**********************************************************
         *  Public Properties
         * ********************************************************/
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string MiddleName
        {
            get { return middleName; }
            set { middleName = value; }
        }

        public string Position
        {
            get { return position; }
            set { position = value; }
        }

        public string Node
        {
            get { return node; }
            set { node = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string ContactNumber {
            get { return contactNumber; }
            set { contactNumber = value; }
        }

        public bool Success
        {
            get { return success; }
            set { success = value; }
        }

        /**********************************************************
         *  Constructor
         * ********************************************************/

        /**********************************************************
         *  Private Methods
         * ********************************************************/

        /**********************************************************
         *  Protected Methods
         * ********************************************************/

        /**********************************************************
         *  Public Methods
         * ********************************************************/

        public void DoLogin()
        {
            success = false;

            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionStringLogin()))
                {
                    //open connection
                    con.Open();

                    //set sql
                    string sql = "SELECT * FROM user WHERE user_username = @username AND user_password = @password AND active = 1";

                    //perpare connection and query string
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    
                    //set parameters
                    cmd.Parameters.AddWithValue("username", username);
                    cmd.Parameters.AddWithValue("password", password);

                    //read rows
                    MySqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        //set this model properties
                        firstName = reader["user_first"].ToString();
                        lastName = reader["user_last"].ToString();
                        middleName = reader["user_middle"].ToString();
                        position = reader["position"].ToString();
                        //email = reader["user_email"].ToString();
                        //contactNumber = reader["user_contact_number"].ToString();
                        id = Int32.Parse(reader["id"].ToString());
                        //set success to true;
                        success = true;
                    }
                }
            }
            catch (MySqlException ex)
            {
                //error in retrieving of user records
               // Messages.Instance.Error(ex.Message.ToString());
                MessageBox.Show(ex.Message.ToString(), "EMR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Verify account before opening the profile
        /// </summary>
        /// <returns></returns>
        public bool DoVerifyAccount()
        {
            success = false;

            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionStringLogin()))
                {
                    //open connection
                    con.Open();

                    //set sql
                    string sql = "SELECT * FROM user WHERE user_username = @username AND user_password = @password AND deleted = 0";

                    //perpare connection and query string
                    MySqlCommand cmd = new MySqlCommand(sql, con);

                    //set parameters
                    cmd.Parameters.AddWithValue("username", username);
                    cmd.Parameters.AddWithValue("password", password);

                    //read rows
                    MySqlDataReader reader = cmd.ExecuteReader();

                    if(reader.HasRows)
                    {
                        //set success to true;
                        success = true;

                        return true;
                    }
                }
            }
            catch (MySqlException ex)
            {
                //error in retrieving of user records
                MessageBox.Show(ex.Message.ToString(), "EMR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return false;
        }

        /// <summary>
        /// Get currently logged in user
        /// </summary>
        public string GetCurrentUser()
        {
            return lastName + ", " + firstName;
        }

    }
}
