﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iSystem
{
    class ItemStatus
    {
        /****************************
        * Protected Properties
        * **************************/
        protected int _id;
        protected string _description;
        protected int _active;
        protected List<ItemStatus> _items = new List<ItemStatus>();
        /****************************
        * Public Properties
        * **************************/
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public int Active
        {
            get { return _active; }
            set { _active = value; }
        }
        /****************************
        * Public Method
        * **************************/
        public void Save()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "INSERT INTO item_status(status_description) VALUES(@desc)";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("desc", _description);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Saved!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public List<ItemStatus> Read()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM item_status WHERE active = @active ORDER BY status_description ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        ItemStatus _item = new ItemStatus();
                        _item._id = int.Parse(dr["id"].ToString());
                        _item._description = dr["status_description"].ToString();
                        _items.Add(_item);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _items;
        }
        public List<ItemStatus> Search()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM item_status WHERE active = @active AND status_description LIKE @desc ORDER BY location_description ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    cmd.Parameters.AddWithValue("desc", "%" + _description + "%");
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        ItemStatus _item = new ItemStatus();
                        _item._id = int.Parse(dr["id"].ToString());
                        _item._description = dr["status_description"].ToString();
                        _items.Add(_item);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _items;
        }
        public List<ItemStatus> GetById()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM item_status WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        ItemStatus _item = new ItemStatus();
                        _item._id = int.Parse(dr["id"].ToString());
                        _item._description = dr["status_description"].ToString();
                        _items.Add(_item);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _items;
        }
        public int GetByName()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM item_status WHERE status_description = @desc";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("desc", _description);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        _id = int.Parse(dr["id"].ToString());
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _id;
        }
        public void Update()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE item_status SET status_description = @desc WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("desc", _description); ;
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Updated!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public void Delete()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE item_status SET active = @active WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("active", _active);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Deleted!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
    }
}
