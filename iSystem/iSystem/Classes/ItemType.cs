﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iSystem
{
    class ItemType
    {
        /****************************
        * Protected Properties
        * **************************/
        protected int _id;
        protected string _description;
        protected string _notes;
        protected int _active;
        protected List<ItemType> _items = new List<ItemType>();
        /****************************
        * Public Properties
        * **************************/
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }
        public int Active
        {
            get { return _active; }
            set { _active = value; }
        }
        /****************************
         * Public Method
         * **************************/
         public void Save()
        {
            try
            {
                using(MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "INSERT INTO item_type(type_description,type_note) VALUES(@desc,@note)";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("desc", _description);
                    cmd.Parameters.AddWithValue("note", _notes);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Saved!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());              
            }
        }
        public List<ItemType> Read()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM item_type WHERE active = @active ORDER BY type_description ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        ItemType it = new ItemType();
                        it._id = int.Parse(dr["id"].ToString());
                        it._description = dr["type_description"].ToString();
                        it._notes = dr["type_note"].ToString();
                        _items.Add(it);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _items;
        }
        public List<ItemType> Search()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM item_type WHERE active = @active AND type_description LIKE @desc OR type_note LIKE @desc ORDER BY type_description ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    cmd.Parameters.AddWithValue("desc", "%" + _description + "%");
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        ItemType it = new ItemType();
                        it._id = int.Parse(dr["id"].ToString());
                        it._description = dr["type_description"].ToString();
                        it._notes = dr["type_note"].ToString();
                        _items.Add(it);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _items;
        }
        public List<ItemType> GetById()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM item_type WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        ItemType it = new ItemType();
                        it._id = int.Parse(dr["id"].ToString());
                        it._description = dr["type_description"].ToString();
                        it._notes = dr["type_note"].ToString();
                        _items.Add(it);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _items;
        }
        public int GetByName()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM item_type WHERE type_description = @desc";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("desc", _description);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {                        
                        _id = int.Parse(dr["id"].ToString());  
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _id;
        }
        public void Update()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE item_type SET type_description = @desc, type_note = @note WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("desc", _description);
                    cmd.Parameters.AddWithValue("note", _notes);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Updated!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public void Delete()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE item_type SET active =@active WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("active", _active);              
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Deleted!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
    }
}
