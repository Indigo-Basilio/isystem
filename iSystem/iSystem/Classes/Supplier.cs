﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iSystem
{
    class Supplier
    {
        /****************************
       * Protected Properties
       * **************************/
        protected int _id;
        protected string _name;
        protected string _detail;
        protected string _note;
        protected int _active;
        protected List<Supplier> suppliers = new List<Supplier>();         
        /****************************
        * Public Properties
        * **************************/
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string Detail
        {
            get { return _detail; }
            set { _detail = value; }
        }
        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }
        public int Active
        {
            get { return _active; }
            set { _active = value; }
        }
        /****************************
       * Public Method
       * **************************/
        public void Save()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "INSERT INTO supplier(supp_name,supp_detail,supp_note) VALUES(@name,@detail,@note)";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("name", _name);
                    cmd.Parameters.AddWithValue("detail", _detail);
                    cmd.Parameters.AddWithValue("note", _note);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Saved!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public List<Supplier> Read()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM supplier WHERE active = @active ORDER BY supp_name ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        Supplier supp = new Supplier();
                        supp._id = int.Parse(dr["id"].ToString());
                        supp._name = dr["supp_name"].ToString();
                        supp._detail= dr["supp_detail"].ToString();
                        supp._note = dr["supp_note"].ToString();
                        suppliers.Add(supp);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return suppliers;
        }
        public List<Supplier> Search()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM supplier WHERE active = @active AND supp_name LIKE @name OR supp_detail LIKE @name ORDER BY supp_name ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    cmd.Parameters.AddWithValue("name", "%" + _name + "%");
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        Supplier supp = new Supplier();
                        supp._id = int.Parse(dr["id"].ToString());
                        supp._name = dr["supp_name"].ToString();
                        supp._detail = dr["supp_detail"].ToString();
                        supp._note = dr["supp_note"].ToString();
                        suppliers.Add(supp);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return suppliers;
        }
        public List<Supplier> GetById()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM supplier WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        Supplier supp = new Supplier();
                        supp._id = int.Parse(dr["id"].ToString());
                        supp._name = dr["supp_name"].ToString();
                        supp._detail = dr["supp_detail"].ToString();
                        supp._note = dr["supp_note"].ToString();
                        suppliers.Add(supp);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return suppliers;
        }
        public int GetByName()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM supplier WHERE supp_name = @name";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("name", _name);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        _id = int.Parse(dr["id"].ToString());
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _id;
        }
        public void Update()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE supplier SET supp_name = @name,supp_detail = @detail, supp_note = @note WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("name", _name);
                    cmd.Parameters.AddWithValue("detail", _detail);
                    cmd.Parameters.AddWithValue("note", _note);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Updated!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public void Delete()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE supplier SET active =@active WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("active", _active);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Deleted!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
    }
}
