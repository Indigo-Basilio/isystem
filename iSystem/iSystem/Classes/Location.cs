﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iSystem
{
    class Location
    {
        /****************************
       * Protected Properties
       * **************************/
        protected int _id;
        protected string _description;
        protected int _active;
        protected List<Location> locations = new List<Location>();
        /****************************
        * Public Properties
        * **************************/
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public int Active
        {
            get { return _active; }
            set { _active = value; }
        }
        /****************************
       * Public Method
       * **************************/
        public void Save()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "INSERT INTO location(location_description) VALUES(@desc)";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("desc", _description);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Saved!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public List<Location> Read()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM location WHERE active = @active ORDER BY location_description ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        Location loc = new Location();
                        loc._id = int.Parse(dr["id"].ToString());
                        loc._description = dr["location_description"].ToString();                        
                        locations.Add(loc);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return locations;
        }
        public List<Location> Search()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM location WHERE active = @active AND location_description LIKE @desc ORDER BY location_description ASC";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("active", _active);
                    cmd.Parameters.AddWithValue("desc", "%" + _description + "%");
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        Location loc = new Location();
                        loc._id = int.Parse(dr["id"].ToString());
                        loc._description = dr["location_description"].ToString();
                        locations.Add(loc);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return locations;
        }
        public List<Location> GetById()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM location WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        Location loc = new Location();
                        loc._id = int.Parse(dr["id"].ToString());
                        loc._description = dr["location_description"].ToString();
                        locations.Add(loc);
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return locations;
        }
        public int GetByName()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "SELECT * FROM location WHERE location_description = @desc";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("desc", _description);
                    //Type of Execute
                    MySqlDataReader dr = cmd.ExecuteReader();
                    //Read
                    while (dr.Read())
                    {
                        _id = int.Parse(dr["id"].ToString());
                    }
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
            return _id;
        }
        public void Update()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE location SET location_description = @desc WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("desc", _description);;
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Updated!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
        public void Delete()
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(iSystem.Configuration.GetConnectionString()))
                {
                    //Open Connection
                    con.Open();
                    //Prepare SQL
                    string sql = "UPDATE location SET active =@active WHERE id = @id";
                    //Command
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    //Parameters
                    cmd.Parameters.AddWithValue("id", _id);
                    cmd.Parameters.AddWithValue("active", _active);
                    //Type of Execute
                    cmd.ExecuteNonQuery();
                    Messages.Instance.Information("Successfully Deleted!");
                }
            }
            catch (MySqlException ex)
            {
                Messages.Instance.Error(ex.Message.ToString());
            }
        }
    }
}
