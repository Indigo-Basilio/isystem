﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iSystem
{
    class DateLibrary
    {
        /* ************************************************
        * Private Properties
        * ***********************************************/
        private static DateLibrary instance = null;

        private static readonly object padlock = new object();

        /*************************************************
         * Protected Properties
         * ***********************************************/

        /*************************************************
         * Public Properties
         * ***********************************************/

        /* ***********************************************
        * Constructor
        * ***********************************************/
        DateLibrary()
        {
        }

        /*************************************************
         * Private Methods
         * ***********************************************/

        /*************************************************
         * Protected Methods
         * ************************************************/

        /*************************************************
         * Public Methods
         * ************************************************/

        /// <summary>
        /// Create a one time instance for this library.
        /// Avoiding multiple instances when called.
        /// </summary>
        public static DateLibrary Instance
        {
            get
            {
                lock (padlock)
                {

                    if (instance == null)
                    {
                        instance = new DateLibrary();
                    }
                    return instance;
                }
            }
        }
        
        /// <summary>
        /// Get current date in time stamp format
        /// </summary>
        /// <returns></returns>
        public string DateNow()
        {
            DateTime date = DateTime.Now;
            return date.ToString("MMM-dd-yyyy HH:mm:ss");
        }

        public string Time()
        {
            return DateTime.Now.ToString("hh:mm:ss tt");
        }

        /// <summary>
        /// Convert string date to DateTime
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public DateTime ConvertDate(string date)
        {
            DateTime dt = Convert.ToDateTime(date);
            return dt;
        }

        public DateTime Now()
        {
            return DateTime.Today;
        }

        /// <summary>
        /// Get Days in a month
        /// </summary>
        /// <param name="year">year in calendar</param>
        /// <param name="month">month in calendar</param>
        /// <returns>DateTime</returns>
        public int DaysInMonth(int year, int month)
        {
            return DateTime.DaysInMonth(year, month);
        }

        /// <summary>
        /// Convert date to specified format
        /// </summary>
        /// <param name="date">date</param>
        /// <param name="format">new format</param>
        /// <returns>string</returns>
        public string ConvertDate(string date, string format)
        {
            //convert string to date
            DateTime dt = Convert.ToDateTime(date);

            if (dt.ToString() == "1/1/0001 12:00:00 AM")
            {
                return "-----";
            }

            return dt.ToString(format);
        }

        /// <summary>
        /// Get the current year based on system time
        /// </summary>
        /// <returns>string</returns>
        public string Year()
        {
            DateTime dt = System.DateTime.Today;
            return dt.ToString("yyyy");
        }

        /// <summary>
        /// Get the current month based on system time
        /// </summary>
        /// <returns>string</returns>
        public string Month()
        {
            DateTime dt = System.DateTime.Today;
            return dt.ToString("MM");
        }

        /// <summary>
        /// Get current timestamp
        /// </summary>
        /// <returns>string</returns>
        public string TimeStamp()
        {
            return DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
        }

        public double TotalDays(DateTime start, DateTime end)
        {
            return (end - start).TotalDays;
        }

        /// <summary>
        /// Add number of days to a date
        /// </summary>
        /// <param name="date"></param>
        /// <param name="numberOfDays"></param>
        /// <returns>string</returns>
        public string AddDays(DateTime date, int numberOfDays)
        {
            DateTime d = date.AddDays(numberOfDays);
            return d.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// Convert a string to DateTime
        /// </summary>
        /// <param name="date"></param>
        /// <returns>DateTime</returns>
        public DateTime ToDateTime(string date)
        {
            return Convert.ToDateTime(date);
        }
    }
}
